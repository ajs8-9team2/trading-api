# Информация по установке

Для установки выполните команды:

```
git clone https://gitlab.com/ajs8-9team2/trading-api.git
cd repo  
npm install  
npm start  
```

Для запуска фикстур выполните команду:

```
npm run fixtures
```

Для запуска базы данных выполните команду:

```
npm run start
```
Для запуска базы данных для разработчика выполните команду: 
```
npm run dev
```

Токен передается в заголовок "Authentication"

```
const token = req.get("Authentication");
```

# Документация Trading  

BASE URL: http://localhost:8000

### POST /users  


JSON payload:
```json
{  
  "email": "string",  
  "password": "string",
  "role" :"string" 
}
```

Response: 
```json
{  
    "_id": "string",
    "email": "string",
    "role": "string",
    "token": "string",
    "__v": 0 
}  
```

### GET /users

Response:
```json  
[  
    {  
        "email": "string",  
        "_id": "string",  
        "token": "string",  
        "__v": 0  
    },  
    {  
        "email": "string",  
        "_id": "string",  
        "token": "string",  
        "__v": 0  
    }  
]
```

### GET /users/:id

Response:
```json  
{  
    "email": "string",  
    "_id": "string",  
    "token": "string",  
    "__v": 0  
},  
```

### POST /users/sessions

JSON payload:
```json
{  
  "email": "string",  
  "password": "string"  
}
```

Response:
```json
{ 
    "_id": "string",
    "email": "string",
    "role": "string",
    "token": "string",
    "__v": 0
}
```

### POST /accRequests

JSON payload:
```json
{
    "_id": "string",
    "user": "string",
    "firstName": "string",
    "lastName": "string",
    "dateOfBirth": "string",
    "indIdentNumber": "string",
    "email": "string",
    "phone": "string",
    "__v": 0
}
```

Response:
```json
{
    "_id": "string",
    "user": "string",
    "status": "Pending",
    "firstName": "string",
    "lastName": "string",
    "dateOfBirth": "string",
    "indIdentNumber": "string",
    "email": "string",
    "phone": "string",
    "__v": 0
}
```

### GET /accRequests

Response: 
```json
{
    "accRequests": [
        {
            "_id": "string",
            "user": {
                "_id": "string",
                "email": "string",
                "role": "string",
                "token": "string",
                "__v": 0
            },
            "status": "string",
            "firstName": "string",
            "lastName": "string",
            "dateOfBirth": "string",
            "indIdentNumber": "string",
            "email": "string",
            "phone": "string",
            "__v": 0
        },
        {
            "_id": "string",
            "user": {
                "_id": "string",
                "email": "string",
                "role": "string",
                "token": "string",
                "__v": 0
            },
            "status": "string",
            "firstName": "string",
            "lastName": "string",
            "dateOfBirth": "string",
            "indIdentNumber": "string",
            "email": "string",
            "phone": "string",
            "patronym": "string",
            "country": "string",
            "area": "",
            "city": "string",
            "address": "string",
            "__v": 0
        }
    ]
}
```

### GET /accRequests/:id 

Response: 
```json
{
    "_id": "string",
    "user": "string",
    "status": "string",
    "firstName": "string",
    "lastName": "string",
    "dateOfBirth": "string",
    "indIdentNumber": "string",
    "email": "string",
    "phone": "string",
    "__v": 0
}
```

### PATCH /accRequests/:id

Строка status принимает значения: ["Pending", "Accepted", "Declined"]

JSON payload:
```json
{  
  "status": "string"
}
```
Response:
```json
{
    "_id": "string",
    "user": "string",
    "status": "Accepted",
    "firstName": "string",
    "lastName": "string",
    "dateOfBirth": "string",
    "indIdentNumber": "string",
    "email": "string",
    "phone": "string",
    "__v": 0
}
```

### POST / accounts

Use with query parameter: /accounts?accreqid=63759c66e29f348bc58771af


###  POST /client-accounts

В Хэдере передаем токен пользователя ключом "Authentication"

JSON payload:
```json
{
    "IBAN": "string",
    "BIK": "string",
    "IIK": "string" ,
    "bank": "string",
    "recipientName": "string",
}
Response:
```json
{
    "user": "string",
    "IBAN": "string",
    "BIK": "string",
    "IIK": "string" ,
    "bank": "string",
    "recipientName": "string",
    "_id": "639804317913c3db24824482",
    "__v": 0
}

  {
    "user": "string",
    "IBAN": "string",
    "BIK": "string",
    "IIK": "string" ,
    "bank": "string",
    "recipientName": "string",
    "_id": "639804317913c3db24824482",
    "__v": 0
  },

###  GET /client-accounts
/* Полуаем все клиентские счета */

[
  {
    "user": "string",
    "IBAN": "string",
    "BIK": "string",
    "IIK": "string" ,
    "bank": "string",
    "recipientName": "string",
    "_id": "639804317913c3db24824482",
    "__v": 0
  },
  {
    "user": "string",
    "IBAN": "string",
    "BIK": "string",
    "IIK": "string" ,
    "bank": "string",
    "recipientName": "string",
    "_id": "639804317913c3db24824482",
    "__v": 0
  }
]

###  GET /client-accounts/:id
/* Полуаем один клиентский счет */
{
    "user": "string",
    "IBAN": "string",
    "BIK": "string",
    "IIK": "string" ,
    "bank": "string",
    "recipientName": "string",
    "_id": "639804317913c3db24824482",
    "__v": 0

}

