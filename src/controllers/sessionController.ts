import { Request, Response } from "express";
import User from "../models/User";

const createSession = async (req: Request, res: Response) => {
    const user = await User.findOne({
        email: req.body.email
    }).populate("account");

    const errorMessage = {
        message: "Неверная почта или пароль"
    };

    if (!user) return res.status(401).send(errorMessage);

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) return res.status(401).send(errorMessage);

    user.generateToken();

    await user.save();

    res.send(user);
};

const deleteSession = async (req: Request, res: Response) => {
    const token = req.get("Authentication");

    if (!token) return res.sendStatus(401);

    const user = await User.findOne({token});

    if (!user) return res.sendStatus(401);

    user.generateToken();

    try {
        await user.save();
        res.sendStatus(204);
    } catch(e) {
        res.status(400).send({message: "Can't logout"});
    }
};

export default {createSession, deleteSession};