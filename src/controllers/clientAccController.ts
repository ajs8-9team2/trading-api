import { Request, Response } from "express";
import ClientAccount from "../models/ClientAccount";
import User from "../models/User";



/* POST /client-accounts   */
/* Создаем один клиентских счет */
const createClientAccount = async (req: Request, res: Response) => {
    const requestData = req.body;

    const token = req.get("Authentication");
    if (!token) return res.status(401).send({message: "Unauthorized, no token"});

    const userOnline = await User.findOne({token});
    if (!userOnline) return res.status(400).send({message: "User not found"});

    requestData.user = userOnline._id;

    const clientAccount  = new ClientAccount({
        // user: requestData.user,
        // IBAN: requestData.IBAN,
        // BIK: requestData.BIK,
        // IIK: requestData.IIK,
        // bank: requestData.bank,
        // recipientName: requestData.recipientName,
        // myIBAN: requestData.myIBAN

        user: requestData.user,
        IIK: requestData.IIK,// принадлежит клиенту
        BIK: requestData.BIK,
        bank: requestData.bank,
        recipientName: requestData.recipientName,
        isMyIIK: requestData.isMyIIK
    });

    try {
        await clientAccount.save();
        res.status(201).send(clientAccount);
    } catch(e) {
        res.status(400).send(e);
    }
};

/* GET /client-accounts   */
/* Полуаем все клиентские счета */
const getClientAccounts = async (req: Request, res: Response) => {
    try {
        const clientAccounts = await ClientAccount.find()
 
        res.status(200).send(clientAccounts);   
    } 
    catch (error) {
        res.status(400).send(error);
    }
};


/* GET /client-accounts/client?clientid=${id}  */
/* Получаем все клиентские счета определенного клиента */ 
const getUsersClientAccount = async (req: Request, res: Response) => {
    try {
        const clientAccounts = await ClientAccount.find({user: req.query.clientid})
        res.status(200).send(clientAccounts);
    } catch (error) {
        res.status(400).send(error);
    }
}; 

/* GET /client-accounts/:id   */
/* Получаем один клиентский счет */
const getOneClientAccount  = async (req: Request, res: Response) => {
    try {
        const clientAccount = await ClientAccount.findById(req.params.id)
        res.status(200).send(clientAccount);
    } catch (error) {
        res.status(400).send(error);
    }
};

export default { createClientAccount, getClientAccounts, getUsersClientAccount, getOneClientAccount };