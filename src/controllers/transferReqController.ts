import { Request, Response } from "express";
import TransferRequest from "../models/TransferRequest";
import Account from "../models/Account";
import User from "../models/User";

const createTransferReq = async (req: Request, res: Response) => {
    const requestData = req.body;

    const token = req.get("Authentication");
    if (!token) return res.status(401).send({message: "Unauthorized, no token"});

    const userOnline = await User.findOne({token});
    if (!userOnline) return res.status(400).send({message: "User not found"});

    requestData.user = userOnline._id;

    const transferReq = new TransferRequest({
        user: requestData.user,
        status: requestData.status,
        senderIIN: requestData.senderIIN,
        recipientIIN: requestData.recipientIIN,
        senderIIK: requestData.senderIIK,
        recipientIIK: requestData.recipientIIK,
        senderBIK: requestData.senderBIK,
        recipientBIK: requestData.recipientBIK,
        senderBank: requestData.senderBank,
        recipientBank: requestData.recipientBank,
        senderKBe: requestData.senderKBe,
        recipientKBe: requestData.recipientKBe,
        senderFullName: requestData.senderFullName,
        recipientFullName: requestData.recipientFullName,
        amount: requestData.amount
    });

    try {
        await transferReq.save();
        res.status(201).send(transferReq);
    } catch(e) {
        res.status(400).send(e);
    }
};

// Все заявки
const getTransferReqs = async (req: Request, res: Response) => {
    try {
        const transferRequests = await TransferRequest.find().sort({date: -1})
            .populate("user")
            .populate("frontManager")
            .populate("accountant");
        res.status(200).send(transferRequests);   
    }
    catch (error) {
        res.status(400).send(error);
    }
};

// Заявки для фронт сотрудников со статусами Pending, FrontWIP
const getTransferReqFront = async (req: Request, res: Response) => {
    try {
        const transferRequests = await TransferRequest.find({status: { $in: [ "Pending", "FrontWIP"]}}).sort({date: -1})
            .populate("user")
            .populate("frontManager")
        res.status(200).send(transferRequests);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки одного фронт сотрудника
const getTransferReqFrontWIP = async (req: Request, res: Response) => {
    try {
        const transferRequests = await TransferRequest.find({$and: [{status: "FrontWIP"}, {frontManager: req.query.frontid}]}).sort({date: -1})
            .populate("user")
            .populate("frontManager")
        res.status(200).send(transferRequests);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки для бухгалтеров со статусами Approved, AccountantWIP
const getTransferReqAccountant = async (req: Request, res: Response) => {
    try {
        const transferRequests = await TransferRequest.find({status: { $in: [ "Approved", "AccountantWIP"]}}).sort({date: -1})
            .populate("user")
            .populate("accountant")
        res.status(200).send(transferRequests);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки одного бухгалтера
const getTransferReqAccountantWIP = async (req: Request, res: Response) => {
    try {
        const transferRequests = await TransferRequest.find({$and: [{status: "AccountantWIP"}, {accountant: req.query.accountantid}]}).sort({date: -1})
            .populate("user")
            .populate("accountant")
        res.status(200).send(transferRequests);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки клиента 
const getTransferReqClient = async (req: Request, res: Response) => {
    try {
        const transferRequests = await TransferRequest.find({user: req.query.clientid}).sort({datetime: -1})
            .populate("user")
            .populate("frontManager")
            .populate("accountant");
        res.status(200).send(transferRequests);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Получить одну заявку
const getTransferReq = async (req: Request, res: Response) => {
    try {
        const transferReq = await TransferRequest.findById(req.params.id)
            .populate("user")
            .populate("frontManager")
            .populate("accountant");
        res.status(200).send(transferReq);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Изменить заявку на вывод из счета по id заявки
// PATCH http://localhost:8000/transferRequests/63808b606dc61e6b36084d9d
const changeTransferReq = async(req: Request, res: Response)=>{
    try{
        const id = req.params.id;
        const reqData = req.body;
//"Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        const transferReqData = await TransferRequest
            .findByIdAndUpdate(id, reqData, {new: true})
            .populate("user")
            .populate("frontManager")
            .populate("accountant");

        // console.log('+++++++++++++++++++++++');
        // console.log(transferReqData?.user._id);
        // если бухгалтер одобрил заявление на вывод из счета
        // то счет автоматически минусуется из Account
        if(transferReqData?.status === "Completed" && transferReqData){
            const accountData = await Account
                .findOne({user: transferReqData.user._id})
                .populate("user");

            if(accountData){
                accountData.amount -= transferReqData.amount;

                await Account
                    .findByIdAndUpdate(accountData._id, accountData, {new: true})
            }
        }

        res.status(200).send(transferReqData);
    }catch{
        res.status(400).send({message: "Transfer request not found"});
    }
};

export default { getTransferReqs, getTransferReq, getTransferReqFront, getTransferReqFrontWIP, getTransferReqAccountant, getTransferReqAccountantWIP, getTransferReqClient, createTransferReq, changeTransferReq };



// const changeTransferReq = async (req: Request, res: Response) => {
//     try {
//         const id = req.params.id;
//         const reqData = req.body;
//         const transferReq = await TransferRequest.findByIdAndUpdate(id, reqData, {new: true})
//             .populate("user")
//             .populate("frontManager")
//             .populate("accountant");
//         res.status(200).send(transferReq);
//     } catch {
//         res.status(400).send({message: "Transfer request not found"});
//     }
// };