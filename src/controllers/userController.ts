import { Request, Response } from "express";
import nodemailer from "nodemailer";
import { nanoid } from "nanoid";
import User from "../models/User";
import Notification from "../models/Notification";

const createUser = async (req: Request, res: Response) => {
    const userData = req.body;
    const userExists = await User.exists({ email: userData.email });

    let mailTransporter = nodemailer.createTransport({
        port: 587,
        secure: false,
        requireTLS: true,
        service: "Gmail",
        auth: {
            user: "brockertrading@gmail.com",
            pass: "xjegrvpapkpwtfyj"
        },
    })

    let details = {
        from: "brockertrading@gmail.com",
        to: userData.email,
        subject: "Trading",
        text: `Уважаемый(ая) ${userData.email}. Благодарим вас, за то что выбрали нашу брокерскую систему. Просим вас заполнить поля для открытие счета.`
    }
    mailTransporter.sendMail(details, (err) => {
        if (err) {
            console.log("it has an error", err);
        }
        else {
            console.log("success")
        }
    })

    try {
        if (userExists === null) {
            userData.token = nanoid();
            const user = new User({
                email: userData.email,
                password: userData.password,
                role: userData.role,
                token: userData.token,
                fullName: userData.fullName,
            });
            await user.save();
            //--------------------------------------------------------
            const notificationData = new Notification({
                user: user,
                userNotifications:[],
            })
            await notificationData.save()
            //--------------------------------------------------------
            return res.status(201).send(user);
        } else {
            return res.status(400).send({ message: "This email is already registered" });
        }
    } catch (e) {
        console.log(e)
        res.status(400).send(e);
    }
};

const getUsers = async (req: Request, res: Response) => {
    try {
        const users = await User.find();
        res.status(200).send(users);
    } catch (error) {
        return res.status(400).send(error);
    }
};

const getUser = async (req: Request, res: Response) => {
    try {
        const user = await User.findById(req.params.id).populate("account");
        res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
};

export default { createUser, getUsers, getUser };