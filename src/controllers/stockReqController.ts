import {Request, Response} from "express";
import DepositRequest from "../models/DepositRequest";
import User from "../models/User";
import StockRequest from "../models/StockRequest";
import { request } from "http";

const createStockReq = async(req: Request, res: Response)=>{
    const requestData = req.body;
    const token = req.get("Authentication");
    if(!token) return res.status(401).send({message: "Unauthorized, no token"});

    const userOnline = await User.findOne({token});
    if(!userOnline) return res.status(400).send({message: "User not found"});

    requestData.user = userOnline._id;
    const stockReq = new StockRequest({
        user: requestData.user,
        userFullName: requestData.userFullName,
        status: requestData.status,
        company: requestData.company,
        ticker: requestData.ticker,
        price: requestData.price,
        stockAmount: requestData.stockAmount,
        stockReqType: requestData.stockReqType
    });

    try{
        await stockReq.save();
        res.status(201).send(stockReq);
    }catch(e){
        res.status(400).send(e);
    }
};


// Все заявки
// GET http://localhost:8000/stock-requests
const getStockReqs = async(req: Request, res: Response)=>{
    try{
        const stockRequests = await StockRequest.find().sort({date: -1})
            .populate("user")
            .populate("frontManager")
            .populate("trader");
        res.status(200).send(stockRequests);
    }catch(error){
        res.status(400).send(error);
    }
};


// Все заявки для фронт сотрудников. Имеют статусы Pending, FrontWIP (работает фронт менеджер)
// GET http://localhost:8000/stock-requests/front
const getStockReqFront = async(req: Request, res: Response)=>{
    try{
        const stockRequests = await StockRequest
            .find({status: {$in: ["Pending", "FrontWIP"]}})
            .sort({date: -1})
            .populate("user")
            .populate("frontManager")
        res.status(200).send(stockRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки одного фронт сотрудника
// Заявки, над которыми работает фронт менеджер. Все имеют статус FrontWIP. Где frontid это id фронт менеджера
// GET http://localhost:8000/stock-requests/frontwip?frontid=${id}
const getStockReqFrontWIP = async(req: Request, res: Response)=>{
    try{
        const stockRequests = await StockRequest
            .find({$and: [{status: "FrontWIP"}, {frontManager: req.query.frontid}]})
            .sort({date: -1})
            .populate("user")
            .populate("frontManager")
        res.status(200).send(stockRequests);
    }catch(error){
        res.status(400).send(error);
    }
};


// Все заявки для трейдеров. Имеют статусы Approved и TraderWIP (работает бухгалтер)
// GET http://localhost:8000/stock-requests/trader
const getStockReqTrader = async(req: Request, res: Response)=>{
    try{
        const stockRequests = await StockRequest
            .find({status: {$in: ["Approved", "TraderWIP"]}})
            .sort({date: -1})
            .populate("user")
            .populate("trader")
        res.status(200).send(stockRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки, над которыми работает один трейдер. Все имеют статус TraderWIP. Где traderid это id трейдера
// GET http://localhost:8000/stock-requests/traderwip?traderid=${id}
const getStockReqTraderWIP = async(req: Request, res: Response)=>{
    try{
        const stockRequests = await StockRequest
            .find({$and: [{status: "TraderWIP"}, {trader: req.query.traderid}]})
            .sort({date: -1})
            .populate("user")
            .populate("trader")
        res.status(200).send(stockRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки клиента
// Заявки конкретного клиента. Где clientid это id клиента
// GET http://localhost:8000/stock-requests/client?clientid=${id}
const getStockReqClient = async(req: Request, res: Response)=>{
    try{
        const stockRequests = await StockRequest
            .find({user: req.query.clientid})
            .sort({datetime: -1})
            .populate("user")
            .populate("frontManager")
            .populate("trader");
        res.status(200).send(stockRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Получить одну заявку по id заявки
// GET http://localhost:8000/stock-requests/${id}
const getStockReq = async(req: Request, res: Response)=>{
    try{
        const stockRequest = await StockRequest
            .findById(req.params.id)
            .populate("user")
            .populate("frontManager")
            .populate("trader");
        res.status(200).send(stockRequest);
    }catch(error){
        res.status(400).send(error);
    }
};

// Изменить заявку на пополнение счета по id заявки
// PATCH http://localhost:8000/stock-requests/${id}
const changeStockReq = async(req: Request, res: Response)=>{
    try{
        const id = req.params.id;
        const reqData = req.body;
        //"Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted"
        const stockReqData = await StockRequest
            .findByIdAndUpdate(id, reqData, {new: true})
            .populate("user")
            .populate("frontManager")
            .populate("trader");
        res.status(200).send(stockReqData);
    }catch{
        res.status(400).send({message: "Request not found"});
    }
};

export default {
    getStockReqs,
    getStockReqFront,
    getStockReq,
    getStockReqFrontWIP,
    getStockReqTrader,
    getStockReqTraderWIP,
    getStockReqClient,
    createStockReq,
    changeStockReq
};

