import {Request, Response} from "express";
import User from "../models/User";
import Notification from "../models/Notification";
// import User from "../models/User";
// https://gitlab.com/ajs8-9team2/trading-api/-/wikis/GET-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B
// https://gitlab.com/ajs8-9team2/trading-api/-/wikis/POST-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B.-PATCH-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B

const createNotification = async(req: Request, res: Response)=>{
    try{
        const userData = await User.findById(req.params.id)

        const isExist = await Notification.exists({user:req.params.id})
        // console.log(isExist," &&&&&&&&&&&&&&&&&")
        const notificationData = new Notification({
            user: userData,
            userNotifications:[],
        })
        await notificationData.save();

        res.status(201).send(notificationData);
    }catch(e){
        return res.sendStatus(400).send(e);
    }
};

// Все уведомления
/** GET http://localhost:8000/notification */
const getNotifications = async(req: Request, res: Response)=>{
    try{
        const accRequests = await Notification
            .find()
            .populate("user");
        res.status(200).send(accRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Уведомление конкретного клиента. Где clientid это id клиента. На данный момент выдает только один счет
/** GET http://localhost:8000/notification/client?clientid=${id} */
const getNotificationsClient = async(req: Request, res: Response)=>{
    try{
        const accRequests = await Notification
            .findOne({user: req.query.clientid})
            .populate("user");
        res.status(200).send(accRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Уведомление по id заявки
/** GET http://localhost:8000/notification/${id} */
const getNotification = async(req: Request, res: Response)=>{
    try{
        const accReq = await Notification
            .findById(req.params.id)
            .populate("user");
        res.status(200).send(accReq);
    }catch(error){
        res.status(400).send(error);
    }
};

// Изменить Notification по id
/** PATCH http://localhost:8000/notification/${id} */
const changeNotification = async(req: Request, res: Response)=>{
    try{
        const id = req.params.id;
        const reqData = req.body;//userNotifications:array[]
        // const isNotificationExists = await Notification;
        // if(await isNotificationExists.exists({_id: id})){//проверка имеется ли соответствующий Notification
            const accountData = await Notification
                .findByIdAndUpdate(id, {userNotifications: reqData}, {new: true})
                .populate("user")

            res.status(200).send(accountData);

        // }else{
        //     res.status(404).send({message: "Notification not found"});
        // }
    }catch{
        res.status(400).send({message: "Notification not found"});
    }
};

// router.post("/", notificationController.createNotification);
// router.get("/client", notificationController.getNotificationsClient);
// router.get("/", notificationController.getNotifications);
// router.get("/:id", notificationController.getNotification);
// router.patch("/:id", notificationController.changeNotification);

export default {getNotifications, getNotification, createNotification, getNotificationsClient, changeNotification};