import {Request, Response} from "express";
import {customAlphabet} from "nanoid";
import Account from "../models/Account";
import AccRequest from "../models/AccRequest";
import DepositRequest from "../models/DepositRequest";
// import User from "../models/User";
// https://gitlab.com/ajs8-9team2/trading-api/-/wikis/GET-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B
// https://gitlab.com/ajs8-9team2/trading-api/-/wikis/POST-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B.-PATCH-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B

const createAccount = async(req: Request, res: Response)=>{
    try{
        if(req.query.accreqid){
            const accReq = await AccRequest.findByIdAndUpdate(req.query.accreqid, {status: "Approved"});
            if(accReq){
                const nanoid = customAlphabet('1234567890', 18)

                const account = new Account({
                    accountNumber: "KZ" + nanoid().toString(),
                    accountType: "KZT",
                    user: accReq.user,
                    firstName: accReq.firstName,
                    lastName: accReq.lastName,
                    dateOfBirth: accReq.dateOfBirth,
                    identificationNumber: accReq.identificationNumber,
                    email: accReq.email,
                    phone: accReq.phone,
                    patronym: accReq.patronym,
                    country: accReq.country,
                    city: accReq.city,
                    area: accReq.area,
                    address: accReq.address,
                    documentFront: accReq.documentFront,
                    documentBack: accReq.documentBack,
                    authority: accReq.authority,
                    dateOfIssue: accReq.dateOfIssue,
                    expirationDate: accReq.expirationDate,
                    idCardNumber: accReq.idCardNumber,
                    amount: "0",
                    citizenship: accReq.citizenship,

                })
                await account.save();

                // теперь все заявки получаем не через юзера, иные поля юзера получаем через account.
                // const updateUser = await User.findByIdAndUpdate(accReq.user, {account : account._id});
                // if(updateUser){
                //     await updateUser.save();
                // }
                res.send(account);
            }else{
                res.status(400).send({message: "Account request not found"});
            }
        }
    }catch(e){
        return res.status(400).send(e);
    }
};


// Все счета
/** GET http://localhost:8000/accounts */
const getAccounts = async(req: Request, res: Response)=>{
    try{
        const accRequests = await Account
            .find()
            .populate("user");
        res.status(200).send(accRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Счет конкретного клиента. Где clientid это id клиента. На данный момент выдает только один счет
/** GET http://localhost:8000/accounts/client?clientid=${id} */
const getAccountsClient = async(req: Request, res: Response)=>{
    try{
        const accRequests = await Account
            .findOne({user: req.query.clientid})
            .populate("user");
        res.status(200).send(accRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Счет по id заявки
/** GET http://localhost:8000/accounts/${id} */
const getAccount = async(req: Request, res: Response)=>{
    try{
        const accReq = await Account
            .findById(req.params.id)
            .populate("user");
        res.status(200).send(accReq);
    }catch(error){
        res.status(400).send(error);
    }
};

// <<< Азамат 03/01/23 С НОВЫМ ГОДОМ! >>>
// Изменить Account по id
/** PATCH http://localhost:8000/accounts/${id} */
// const changeAccount = async(req: Request, res: Response)=>{
//     try{
//         const id = req.params.id;
//         const reqData = req.body;//userNotifications:array[]
//         const isAccountExists = Account;
//         if(await isAccountExists.exists({_id: id})){//проверка имеется ли соответствующий Account
//             const accountData = await Account
//                 .findByIdAndUpdate(id, {userNotifications: reqData}, {new: true})
//                 // .sort({ datetime: -1 })
//                 .populate("user")
//                 // .find({ user: req.query.clientid })
//
//             res.status(200).send(accountData);
//         }else{
//             res.status(404).send({message: "Account not found"});
//         }
//     }catch{
//         res.status(400).send({message: "Account not found"});
//     }
// };

// router.post("/", accountsController.createAccount);
// router.get("/client", accountsController.getAccountsClient);
// router.get("/", accountsController.getAccounts);
// router.get("/:id", accountsController.getAccount);

export default {getAccounts, getAccount, createAccount, getAccountsClient/*, changeAccount*/};