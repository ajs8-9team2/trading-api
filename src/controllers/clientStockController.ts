import { Request, Response } from "express";
import User from "../models/User";
import ClientStock from "../models/ClientStock";

const createStock = async (req: Request, res: Response) => {
    const stockData = req.body;
    const userExists = await User.exists({ _id: stockData.user });
    try {
        if (userExists !== null) {
            const stockExists = await ClientStock.findOne({$and: [{user: stockData.user}, {company: stockData.company}]});
            if (stockExists && stockData.stockReqType === "Buy") {
                stockExists.amount += stockData.amount;
                await ClientStock.findByIdAndUpdate(stockExists._id, stockExists, {new: true});
            } else if (stockExists && stockData.stockReqType === "Sell") {
                stockExists.amount -= stockData.amount;
                await ClientStock.findByIdAndUpdate(stockExists._id, stockExists, {new: true});
            }
            if (!stockExists) {
                const stock = new ClientStock({
                    user: stockData.user,
                    company: stockData.company,
                    ticker: stockData.ticker,
                    amount: stockData.amount
                });
                await stock.save();
                return res.status(201).send(stock);
            }
        } else {
            return res.status(400).send({ message: "Пользователь не найден" });
        }
    } catch (e) {
        console.log(e)
        res.status(400).send(e);
    }
};


// Все акции клиентов
// GET http://localhost:8000/client-stocks
const getStocks = async (req: Request, res: Response) => {
    try{
        const stocks = await ClientStock.find()
            .populate("user")
        res.status(200).send(stocks);
    }catch(error){
        res.status(400).send(error);
    }
};

// Акции конкретного клиента. Где clientid это id клиента
// GET http://localhost:8000/client-stocks/client?clientid=${id}

const getClientStocks = async (req: Request, res: Response) => {
    try{
        const clientStocks = await ClientStock
            .find({user: req.query.clientid})
            .sort({datetime: 1})
            .populate("user")
        res.status(200).send(clientStocks);
    }catch(error){
        res.status(400).send(error);
    }
};

// Получить одну  клиентскую акцию по id
// GET http://localhost:8000/client-stocks/${id}
const getStock = async(req: Request, res: Response)=>{
    try{
        const stock = await ClientStock
            .findById(req.params.id)
            .populate("user")
        res.status(200).send(stock);
    }catch(error){
        res.status(400).send(error);
    }
};

export default { createStock, getStocks, getClientStocks, getStock };