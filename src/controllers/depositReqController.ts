import {Request, Response} from "express";
import DepositRequest from "../models/DepositRequest";
import User from "../models/User";
import Account from "../models/Account";
// https://gitlab.com/ajs8-9team2/trading-api/-/wikis/GET-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B
// https://gitlab.com/ajs8-9team2/trading-api/-/wikis/POST-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B.-PATCH-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B

const createDepositReq = async(req: Request, res: Response)=>{
    const requestData = req.body;

    const token = req.get("Authentication");
    if(!token) return res.status(401).send({message: "Unauthorized, no token"});

    const userOnline = await User.findOne({token});
    if(!userOnline) return res.status(400).send({message: "User not found"});

    requestData.user = userOnline._id;

    const depositReq = new DepositRequest({
        user: requestData.user,
        status: requestData.status,
        senderIIN: requestData.senderIIN,
        senderIIK: requestData.senderIIK,
        senderBIK: requestData.senderBIK,
        senderBank: requestData.senderBank,
        senderKBe: requestData.senderKBe,
        senderFullName: requestData.senderFullName,
        amount: requestData.amount
    });

    try{
        await depositReq.save();
        res.status(201).send(depositReq);
    }catch(e){
        res.status(400).send(e);
    }
};


// Все заявки
// GET http://localhost:8000/depositRequests
const getDepositReqs = async(req: Request, res: Response)=>{

    try{
        const depositRequests = await DepositRequest.find().sort({date: -1})
            .populate("user")
            .populate("frontManager")
            .populate("accountant");
        res.status(200).send(depositRequests);
    }catch(error){
        res.status(400).send(error);
    }
};


// Все заявки для фронт сотрудников. Имеют статусы Pending, FrontWIP (работает фронт менеджер)
// GET http://localhost:8000/depositRequests/front
const getDepositReqFront = async(req: Request, res: Response)=>{
    try{
        const depositRequests = await DepositRequest
            .find({status: {$in: ["Pending", "FrontWIP"]}})
            .sort({date: -1})
            .populate("user")
            .populate("frontManager")

        res.status(200).send(depositRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки одного фронт сотрудника
// Заявки, над которыми работает фронт менеджер. Все имеют статус FrontWIP. Где frontid это id фронт менеджера
// GET http://localhost:8000/depositRequests/frontwip?frontid=${id}
const getDepositReqFrontWIP = async(req: Request, res: Response)=>{
    try{
        const depositRequests = await DepositRequest
            .find({$and: [{status: "FrontWIP"}, {frontManager: req.query.frontid}]})
            .sort({date: -1})
            .populate("user")
            .populate("frontManager")
        res.status(200).send(depositRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки для бухгалтеров со статусами Approved, AccountantWIP
// Все заявки для бухгалтеров. Имеют статусы Approved и AccountantWIP (работает бухгалтер)
// GET http://localhost:8000/depositRequests/accountant
const getDepositReqAccountant = async(req: Request, res: Response)=>{
    try{
        const depositRequests = await DepositRequest
            .find({status: {$in: ["Approved", "AccountantWIP"]}})
            .sort({date: -1})
            .populate("user")
            .populate("accountant")
        res.status(200).send(depositRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки одного бухгалтера
// Заявки, над которыми работает бухгалтер. Все имеют статус AccountantWIP. Где accountantid это id бухгалтера
// GET http://localhost:8000/depositRequests/accountantwip?accountantid=${id}
const getDepositReqAccountantWIP = async(req: Request, res: Response)=>{
    try{
        const depositRequests = await DepositRequest
            .find({$and: [{status: "AccountantWIP"}, {accountant: req.query.accountantid}]})
            .sort({date: -1})
            .populate("user")
            .populate("accountant")
        res.status(200).send(depositRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Заявки клиента
// Заявки конкретного клиента. Где clientid это id клиента
// GET http://localhost:8000/depositRequests/client?clientid=${id}
const getDepositReqClient = async(req: Request, res: Response)=>{
    try{
        const depositRequests = await DepositRequest
            .find({user: req.query.clientid})
            .sort({datetime: -1})
            .populate("user")
            .populate("frontManager")
            .populate("accountant");
        res.status(200).send(depositRequests);
    }catch(error){
        res.status(400).send(error);
    }
};

// Получить одну заявку по id заявки
// GET http://localhost:8000/depositRequests/${id}
const getDepositReq = async(req: Request, res: Response)=>{
    try{
        const depositRequest = await DepositRequest
            .findById(req.params.id)
            .populate("user")
            .populate("frontManager")
            .populate("accountant");
        res.status(200).send(depositRequest);
    }catch(error){
        res.status(400).send(error);
    }
};

// Изменить заявку на пополнение счета по id заявки
// PATCH http://localhost:8000/depositRequests/63808b606dc61e6b36084d9d
const changeDepositReq = async(req: Request, res: Response)=>{
    try{
        const id = req.params.id;
        const reqData = req.body;
//"Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        const depositReqData = await DepositRequest
            .findByIdAndUpdate(id, reqData, {new: true})
            .populate("user")
            .populate("frontManager")
            .populate("accountant");

        // console.log('+++++++++++++++++++++++');
        // console.log(depositReqData?.user._id);
        // если бухгалтер одобрил заявление на пополнение счета
        // то счет автоматически пополняется в Account
        if(depositReqData?.status === "Completed" && depositReqData){
            const accountData = await Account
                .findOne({user: depositReqData.user._id})
                .populate("user");

            if(accountData){
                accountData.amount += depositReqData.amount;

                await Account
                    .findByIdAndUpdate(accountData._id, accountData, {new: true})
            }
        }

        res.status(200).send(depositReqData);
    }catch{
        res.status(400).send({message: "Deposit request not found"});
    }
};

export default {
    getDepositReqs,
    getDepositReqFront,
    getDepositReq,
    getDepositReqFrontWIP,
    getDepositReqAccountant,
    getDepositReqAccountantWIP,
    getDepositReqClient,
    createDepositReq,
    changeDepositReq
};

// router.get("/front", depositReqController.getDepositReqFront);
// router.get("/frontwip", depositReqController.getDepositReqFrontWIP);
// router.get("/accountant", depositReqController.getDepositReqAccountant);
// router.get("/accountantwip", depositReqController.getDepositReqAccountantWIP);
// router.get("/client", depositReqController.getDepositReqClient);
// router.post("/", depositReqController.createDepositReq);
// router.patch("/:id", depositReqController.changeDepositReq);
// router.get("/:id", depositReqController.getDepositReq);
// router.get("/", depositReqController.getDepositReqs);
