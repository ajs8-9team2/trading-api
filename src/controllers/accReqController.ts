import { Request, Response } from "express";
import nodemailer from "nodemailer";
import AccRequest from "../models/AccRequest";
import User from "../models/User";

// Все заявки
// http://localhost:8000/accRequests
const getAccReqs = async (req: Request, res: Response) => {
    try {
        const accRequestData = await AccRequest
            .find()
            .sort({ date: -1 })
            .populate("user")
            .populate("assignedTo")
        res.status(200).send(accRequestData);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки со статусами Pending, WorkInProgress
// http://localhost:8000/accRequests/front
const getAccReqFront = async (req: Request, res: Response) => {
    try {
        const accRequestData = await AccRequest
            .find({ status: { $in: ["Pending", "WorkInProgress"] } })
            .sort({ datetime: -1 })
            .populate("user")
            .populate("assignedTo");
        res.status(200).send(accRequestData);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки, над которыми работает фронт-менеджер, только со статусом WorkInProgress Declined
// http://localhost:8000/accRequests/frontwip?frontid=${id}
const getAccReqFrontWIP = async (req: Request, res: Response) => {
    // console.log(req.query)
    try {
        const accRequestData = await AccRequest.find({$and: [{status: "WorkInProgress"}, {assignedTo: req.query.frontid}]}).sort({datetime: -1})
        // const accRequestData = await AccRequest.find({ $and: [{ status: { $in: ["Declined", "WorkInProgress"] } }, { assignedTo: req.query.frontid }] })
            .sort({ datetime: -1 })
            .populate("user")
            .populate("assignedTo");
        res.status(200).send(accRequestData);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Заявки клиента
// http://localhost:8000/accRequests/client?clientid=639deb1218b0db88d392eeaa
const getAccReqClient = async (req: Request, res: Response) => {
    try {
        const accRequestData = await AccRequest
            .find({ user: req.query.clientid })
            .sort({ datetime: -1 })
            .populate("user")
            .populate("assignedTo");
        res.status(200).send(accRequestData);
    } catch (error) {
        res.status(400).send(error);
    }
};

// Получить одну заявку
// http://localhost:8000/accRequests/63808b606dc61e6b36084d9d
const getAccReq = async (req: Request, res: Response) => {
    try {
        const accRequestData = await AccRequest
            .findById(req.params.id)
            .populate("user")
            .populate("assignedTo");
        res.status(200).send(accRequestData);
    } catch (error) {
        res.status(400).send(error);
    }
};

const changeAccReqStatus = async (req: Request, res: Response) => {
    try {
        const id = req.params.id;
        const accReqData = req.body;
        const accRequestData = await AccRequest
            .findByIdAndUpdate(id, accReqData, { new: true })
            .populate("user")
            .populate("assignedTo");

        // console.log(accReqData," <<<<")

        if (accReqData?.status === "Approved") {
            let innerStr: any;
            if (accRequestData) {
                innerStr = accRequestData.user;
            }

            // находим пользователя по id чтоб поменять ему путь в ключе path на "client-office"
            const userData = await User.findById(innerStr._id as string)

            if (userData) {
                userData.path = "client-office" //"account-opening" | "acc-pending" | "client-office"
                userData.isHasAccount = true;

                // console.log(userData," >>>>")
                await User.findByIdAndUpdate(innerStr._id, userData, { new: true })

                let mailTransporter = nodemailer.createTransport({
                    port: 587,
                    secure: false,
                    requireTLS: true,
                    service: "Gmail",
                    auth: {
                        user: "brockertrading@gmail.com",
                        pass: "xjegrvpapkpwtfyj"
                    },
                })

                let details = {
                    from: "brockertrading@gmail.com",
                    to: userData.email,
                    subject: "Trading",
                    text: `Уважаемый(ая) ${accRequestData?.firstName}, ваша заявка на открытие счета принята сотрудником фронт-офиса. Можете зайти на свой личный кабинет.`
                }
                mailTransporter.sendMail(details, (err) => {
                    if (err) {
                        console.log("it has an error", err);
                    }
                    else {
                        console.log("success")
                    }
                })
            }
        }

        res.status(200).send(accRequestData);
    } catch {
        res.status(400).send({ message: "Account request not found" });
    }
};

export default { getAccReqs, getAccReq, changeAccReqStatus, getAccReqFrontWIP, getAccReqFront, getAccReqClient };