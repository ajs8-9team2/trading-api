import path from "path";

const rootPath = process.cwd();

console.log("root" + rootPath)

export default {
    rootPath,
    uploadPath: path.join(rootPath, "public", "uploads"),
    db: {
        url: "mongodb://localhost/",
        name: "Trading"
    }
};
