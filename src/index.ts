import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import users from "./routes/users";
import accRequests from "./routes/accRequests";
import accounts from "./routes/accounts";
import depositRequests from "./routes/depositRequests";
import transferRequests from "./routes/transferRequests";
import clientAccounts from "./routes/clientAccounts";
import notification from "./routes/notification";
import clientStocks from "./routes/clientStocks";
import stockRequests from "./routes/stockRequests";

//--------------------------------------------------------
import enableWs from "express-ws";
const app = express();
enableWs(app)//важно до импорта роута!!!!!! этого require("./routes/chat");
import chat from "./routes/notificationWS";
// const app = express();
const port = 8000;

app.use(cors());
app.use(express.static("public"));
app.use(express.json());
app.use("/users", users);
app.use("/accRequests", accRequests); 
app.use("/accounts", accounts);
app.use("/depositRequests", depositRequests);
app.use("/transferRequests", transferRequests);
app.use("/client-accounts", clientAccounts);
app.use("/client-stocks", clientStocks);
app.use("/stock-requests", stockRequests);
app.use("/notifications", notification);
app.use("/",chat);

const run = async () => {
    await mongoose.connect("mongodb://localhost/Trading");
        console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(
            `Server started at http://localhost:${port}`
        );
    });

    process.on("exit", () => {
        mongoose.disconnect();
    });
};

run().catch(console.log);


