import {IUser} from "./models/User";
import mongoose from "mongoose";

// Utility types in TS
// type TOmitUser = Omit<IUser>
export type TPickUser = Pick<IUser, "email" | "role" | "password" | "token" | "isHasAccount" | "fullName">;
export type TUserType = TPickUser & { _id: mongoose.Types.ObjectId };