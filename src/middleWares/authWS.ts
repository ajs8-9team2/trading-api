import {Document, Types} from "mongoose";
import User, {IUser} from "../models/User";

type TWS = {
    send: (arg0: string)=>any;
}

type TReq = {
    query: { token: any; };
    user: Document<unknown, any, IUser> & IUser & { _id: Types.ObjectId; };
    // user: IUser;
}

const authWS = async(
    ws:TWS,
    req: TReq,
    next: ()=>void
)=>{
    const {token} = req.query;
    if(!token){
        return ws.send(JSON.stringify({
            type: "NO_TOKEN",
            error: {message: "No token present"}
        }));
    }
    const user = await User.findOne({token});
    if(!user){
        return ws.send(JSON.stringify({
            type: "INCORRECT_TOKEN",
            error: {message: "Wrong token"}
        }));
    }
    req.user = user;
    next();
}

export default authWS