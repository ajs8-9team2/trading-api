//***************************************************************************
/*
            ws://localhost:8000/chat?token=WWj25teXW-ssdgg1b3fYi
            63aae1b5cb8e958632cb7bc0
            client5@mail.com
            5PC--pCFiLI_d2eUWT8PG
            WWj25teXW-ssdgg1b3fYi
            WWj25teXW-ssdgg1b3fYi

            ws://localhost:8000/chat?token=BWGewxEZaR5tk0RfQMpQt
            63aae1b5cb8e958632cb7bc5
            client10@mail.com
            BWGewxEZaR5tk0RfQMpQt

            //-------------------------------------
            // response
            {
                "type":"NEW_MESSAGE",
                "ID":{
                    "username":"client10@mail.com",
                    "senderUserID":"63aae1b5cb8e958632cb7bc5",
                    "userID":"63aae1b5cb8e958632cb7bc5"
                },
                "message":{
                    "message":"pidorok"
                }
            }
            //-------------------------------------
            // request
            // "accRequest", "depositRequest", "transferRequest",
            // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
            {
                "type": "MESSAGE_CREATED",
                "userID": "63aae1b5cb8e958632cb7bc0",
                "status": "Approved",
                "typeOfRequest":"transferRequest",
                "message": "ах тыж жопа!666"
            }

            {
                "type": "MESSAGE_CREATED",
                "userID": "63bcfbb1a072d68817b9cabb",
                "status": "Approved",
                "typeOfRequest":"transferRequest",
                "notice": "кто пойдет на праздник 1",
                "requestID": "63bcfbc4a072d68817b9d030",
            }

        */
//***************************************************************************
import express from "express";
import User from "../models/User";
import Notification, {TNoticeObj} from "../models/Notification";

const router = express.Router();
// тип различных информативных сообщений высылаемые для сотрудника
// в случае неуспешных уведомлений
type TResMessageForEmployee = {
    type: "NEW_MESSAGE";
    ID?: {
        username: string;
        senderUserID: string;// ID кому высылают сообщение
        userID: string;// ID кто высылает сообщение
    };
    notice: string;
}

// тип для сообщений, которые приходят от сотрудника
type TReqMessage =
// Pick<TNoticeObj, "status"|"typeOfRequest"|"notice"|"requestID">
// &
    {
// /*!*/ status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed",
    /*!*/ status: "Pending" | "FrontWIP" | "AccountantWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted",
// /*!*/ typeOfRequest: "accRequest" | "depositRequest" | "transferRequest",
    /*!*/ typeOfRequest: "accRequest" | "depositRequest" | "transferRequest" | "stockRequest",
    /*!*/ notice: string
    /*!*/ requestID: string,

    /*?*/ type: "MESSAGE_CREATED" | "NEW_MESSAGE",
    /*?*/ userID: string,
}

const activeConnections: any = {};
// в случае неудачных высылок сообщений
const responseObject = (decodedMessage: TReqMessage, username: string, id: string, notice = ""): TResMessageForEmployee=>{
    return {
        type: "NEW_MESSAGE",
        ID: {
            username,
            senderUserID: id,// id кто высылает сообщение
            userID: decodedMessage.userID,// id кто получает сообщение
        },

        notice: (()=>{
            if(!!notice)
                return notice
            else
                return decodedMessage.notice
        })()
        // decodedMessage.message
    };
}

router.ws("/chat", async(webSocket, req)=>{
    const {token} = req.query;
    if(!token){
        return webSocket.send(JSON.stringify({
            type: "NO_TOKEN",
            error: {message: "No token present"}
        }));
    }
    const user = await User.findOne({token: token});
    if(!user){
        return webSocket.send(JSON.stringify({
            type: "INCORRECT_TOKEN",
            error: {message: `Wrong token ${token}`}
        }));
    }
//###############################################################################################
    const id = user._id.toString();//nanoid();
    activeConnections[id] = webSocket;
    let username = user.email;// "";
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    webSocket.on("message", async(message)=>{
        let decodedMessage: TReqMessage | undefined = undefined;
        try{
            decodedMessage = JSON.parse(message as unknown as string) as TReqMessage;
            // console.log(JSON.stringify(decodedMessage))
        }catch(error: any){
            webSocket.send(JSON.stringify({
                type: "NEW_MESSAGE",
                ID: {
                    username,
                    senderUserID: id,
                },
                message: "ошибка при парсинге"
            }));

            // const xcb = "http://stackoverflow.com/search?q=[js]+"+error.message;
            // window.open(xcb,'_blank')
        }

        if(!!decodedMessage)
            switch(decodedMessage.type){
                case "MESSAGE_CREATED":
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                    const innerObjData: TNoticeObj = {
                        // type: "NEW_MESSAGE",
                        date: new Date(),
                        notice: decodedMessage.notice,
                        status: decodedMessage.status,
                        typeOfRequest: decodedMessage.typeOfRequest,
                        isWasRead: false,
                        requestID: decodedMessage.requestID,
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    const notificationData = await Notification.findOne({user: decodedMessage?.userID})
                        .populate("user");
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    /** было ли одобрено подобное сообщение ранее, если да, то в базу уведомление не сохраняем
                     typeOfRequest: "accRequest", "depositRequest", "transferRequest", "stockRequest"*/
                    const isHasTypeOfRequest: boolean = notificationData?.userNotifications
                        ?.some(item=>item.typeOfRequest === decodedMessage?.typeOfRequest) as boolean;


                    /** имеется ли клиент в онлайне? если да, то выполняем эту часть кода */
                    if(Object.keys(activeConnections).includes(decodedMessage?.userID)){
                        try{
                            // if(!isHasTypeOfRequest){
                            notificationData?.userNotifications?.push(innerObjData);

                            if(notificationData)
                                await notificationData.save();
                            // если возникла ошибка при сохранении данных, то сообщение ниже не высылается
                            activeConnections[decodedMessage.userID]
                                .send(JSON.stringify({...innerObjData, type: "NEW_MESSAGE"}));

                            // }else{
                            //     //похожее сообщение ранее уже было одобрено/отклонено
                            //     const innerObj: TResMessageForEmployee = responseObject(decodedMessage, username, id, "похожее сообщение ранее уже было одобрено/отклонено" + decodedMessage.typeOfRequest);
                            //     activeConnections[id].send(JSON.stringify(innerObj));
                            // }
                        }catch(error){
                            //ошибка при сохранении данных - ошибка 500
                            const innerObj: TResMessageForEmployee = responseObject(decodedMessage, username, id, "ошибка при сохранении данных - ошибка 500");
                            activeConnections[id].send(JSON.stringify(innerObj));
                        }
                    }else{
                        /** если нет, то выполняем эту часть кода */
                        try{
                            // if(!isHasTypeOfRequest){
                            notificationData?.userNotifications?.push(innerObjData);
                            if(notificationData)
                                await notificationData.save();

                            // }else{
                            //     //похожее сообщение ранее уже было одобрено/отклонено
                            //     const innerObj: TResMessageForEmployee = responseObject(decodedMessage, username, id, "похожее сообщение ранее уже было одобрено/отклонено " + decodedMessage.typeOfRequest);
                            //     activeConnections[id].send(JSON.stringify(innerObj));
                            // }
                        }catch(error){
                            //ошибка при сохранении данных - ошибка 500
                            const innerObj: TResMessageForEmployee = responseObject(decodedMessage, username, id, "ошибка при сохранении данных - ошибка 500");
                            activeConnections[id].send(JSON.stringify(innerObj));
                        }

                        // const innerObj: TResMessageForEmployee = responseObject(decodedMessage, username, id, "клиент не в онлайне!");
                        // activeConnections[id].send(JSON.stringify(innerObj));
                    }
                    break;
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                default:
                    console.log("Unknown message type: " + decodedMessage.type);
            }
    });
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    webSocket.on("close", async(msg)=>{
        console.log("Client disconnected " + id);
        // user.isOnline = false;
        // await user.save({validateBeforeSave:false})//чтоб не сохранять юзера повторно
        // delete activeConnections[id];
    });
});

export = router;
