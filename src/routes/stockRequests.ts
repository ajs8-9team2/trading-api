import express from "express";
import stockReqController from "../controllers/stockReqController";

const router = express.Router();

router.get("/front", stockReqController.getStockReqFront);
router.get("/frontwip", stockReqController.getStockReqFrontWIP);
router.get("/trader", stockReqController.getStockReqTrader);
router.get("/traderwip", stockReqController.getStockReqTraderWIP);
router.get("/client", stockReqController.getStockReqClient);
router.post("/", stockReqController.createStockReq);
router.patch("/:id", stockReqController.changeStockReq);
router.get("/:id", stockReqController.getStockReq);
router.get("/", stockReqController.getStockReqs);

export = router;