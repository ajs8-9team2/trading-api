import express from "express";
import transferReqController from "../controllers/transferReqController";

const router = express.Router();

router.get("/front", transferReqController.getTransferReqFront);
router.get("/frontwip", transferReqController.getTransferReqFrontWIP);
router.get("/accountant", transferReqController.getTransferReqAccountant);
router.get("/accountantwip", transferReqController.getTransferReqAccountantWIP);
router.get("/client", transferReqController.getTransferReqClient);
router.post("/", transferReqController.createTransferReq);
router.patch("/:id", transferReqController.changeTransferReq);
router.get("/:id", transferReqController.getTransferReq);
router.get("/", transferReqController.getTransferReqs);

export = router;