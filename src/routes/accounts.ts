import express from "express";
import accountsController from "../controllers/accountsController";

const router = express.Router();

router.post("/", accountsController.createAccount);
router.get("/client", accountsController.getAccountsClient);
router.get("/", accountsController.getAccounts);
router.get("/:id", accountsController.getAccount);
// router.patch("/:id", accountsController.changeAccount);

export = router;