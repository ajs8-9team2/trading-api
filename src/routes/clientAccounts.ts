import express from "express";
import clientAccController from "../controllers/clientAccController";

const router = express.Router();

router.post("/", clientAccController.createClientAccount);
router.get("/", clientAccController.getClientAccounts);
router.get("/client", clientAccController.getUsersClientAccount);
router.get("/:id", clientAccController.getOneClientAccount);


export = router;