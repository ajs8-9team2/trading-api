import express, {Request, Response} from "express";
import accReqController from "../controllers/accReqController";
import multer from "multer";
import config from "../config";
import {nanoid} from "nanoid";
import path from "path";
import User from "../models/User";
import AccRequest from "../models/AccRequest";

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb)=>{
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb)=>{
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const multipleUpload = upload.fields([
    {name: "documentFront", maxCount: 1},
    {name: "documentBack", maxCount: 1}
])


router.post("/", multipleUpload, async(req: Request, res: Response)=>{
    const accReqData = req.body;

    // console.log(accReqData, " <<<<<<<<<<<<")

    const token = req.get("Authentication");
    if(!token) return res.status(401);

    const userOnline = await User.findOneAndUpdate({token}, {path:"acc-pending" });
    if(!userOnline) return res.sendStatus(401);

    accReqData.user = userOnline._id;


    const accReq = new AccRequest({
        user: accReqData.user,
        firstName: accReqData.firstName,
        lastName: accReqData.lastName,
        dateOfBirth: accReqData.dateOfBirth,
        identificationNumber: accReqData.identificationNumber,
        email: accReqData.email,
        phone: accReqData.phone,
        patronym: accReqData.patronym,
        country: accReqData.country,
        city: accReqData.city,
        area: accReqData.area,
        address: accReqData.address,
        date: accReqData.date,
        authority: accReqData.authority,
        dateOfIssue: accReqData.dateOfIssue,
        expirationDate: accReqData.expirationDate,
        documentFront: accReqData.documentFront,
        documentBack: accReqData.documentBack,
        idCardNumber: accReqData.idCardNumber,
        citizenship:'resident', //accReqData.citizenship,
    });

    const files = req.files as { [fieldname: string]: Express.Multer.File[] };

    if(files){
        accReq.documentFront = files["documentFront"][0].filename;
        accReq.documentBack = files["documentBack"][0].filename;
    }

    try{
        await userOnline.save();
        await accReq.save();

        res.status(201).send(accReq);

    }catch(e){
        console.log(e)
        res.status(400).send(e);
    }
});

router.get("/frontwip", accReqController.getAccReqFrontWIP);
router.get("/front", accReqController.getAccReqFront);
router.get("/client", accReqController.getAccReqClient);
router.get("/:id", accReqController.getAccReq);
router.patch("/:id", accReqController.changeAccReqStatus);
router.get("/", accReqController.getAccReqs);

export = router;