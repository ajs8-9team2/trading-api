import express from "express";
import notificationController from "../controllers/notificationController";

const router = express.Router();

router.post("/", notificationController.createNotification);
router.get("/client", notificationController.getNotificationsClient);
router.get("/", notificationController.getNotifications);
router.get("/:id", notificationController.getNotification);
router.patch("/:id", notificationController.changeNotification);

export = router;