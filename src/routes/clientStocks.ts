import express from "express";
import clientStockController from "../controllers/clientStockController";

const router = express.Router();

router.get("/client", clientStockController.getClientStocks);
router.get("/:id", clientStockController.getStock);
router.post("/", clientStockController.createStock);
router.get("/", clientStockController.getStocks);

export = router;