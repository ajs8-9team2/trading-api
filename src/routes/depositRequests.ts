import express from "express";
import depositReqController from "../controllers/depositReqController";

const router = express.Router();

router.get("/front", depositReqController.getDepositReqFront);
router.get("/frontwip", depositReqController.getDepositReqFrontWIP);
router.get("/accountant", depositReqController.getDepositReqAccountant);
router.get("/accountantwip", depositReqController.getDepositReqAccountantWIP);
router.get("/client", depositReqController.getDepositReqClient);
router.post("/", depositReqController.createDepositReq);
router.patch("/:id", depositReqController.changeDepositReq);
router.get("/:id", depositReqController.getDepositReq);
router.get("/", depositReqController.getDepositReqs);

export = router;