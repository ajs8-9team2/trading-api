import express from "express";
import userController from "../controllers/userController";
import sessionController from "../controllers/sessionController";

const router = express.Router();

router.get("/", userController.getUsers);
router.post("/", userController.createUser);
router.get("/:id", userController.getUser);
router.post("/sessions", sessionController.createSession);
router.delete("/sessions", sessionController.deleteSession);

export = router;