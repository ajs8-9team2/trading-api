import {getRandomInt} from "./functionHelpers/randomizers";
import {IAccReq} from "../models/AccRequest";
import {IAccount} from "../models/Account";
import {TUserType} from "../DataTypes";

export function createAccountFixtures(arrayItem: IAccReq[],arrayUser:TUserType[]): IAccount[]{
    const innerArray: IAccount[] = [];

    for(const arrayItemElement of arrayItem){
        if(arrayItemElement.status !== "Approved") continue;
        let isFlag = false;
        for(const userElement of arrayUser){
            if(arrayItemElement.user._id === userElement._id)
                isFlag = userElement.isHasAccount
        }
        // если нет аккаунта, то пропускаем цикл
        if(!isFlag)continue;

        innerArray.push({
            user: arrayItemElement.user,
            // "KZ_709748_396977_478361",
            accountNumber: `KZ${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}`,
            accountType: "KZT",
            firstName: arrayItemElement.firstName,
            lastName: arrayItemElement.lastName,
            patronym: arrayItemElement.patronym,
            dateOfBirth: arrayItemElement.dateOfBirth,
            identificationNumber: arrayItemElement.identificationNumber,
            email: arrayItemElement.email,
            phone: arrayItemElement.phone,
            country: arrayItemElement.country,
            area: arrayItemElement.area,
            city: arrayItemElement.city,
            address: arrayItemElement.address,
            documentFront: arrayItemElement.documentFront,
            documentBack: arrayItemElement.documentBack,
            authority: arrayItemElement.authority,
            dateOfIssue: arrayItemElement.dateOfIssue,
            expirationDate: arrayItemElement.expirationDate,
            idCardNumber: arrayItemElement.idCardNumber,
            amount: 0,
            citizenship: arrayItemElement.citizenship,
        })
    }

    return innerArray
}
