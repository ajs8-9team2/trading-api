import * as mongoose from "mongoose";
import {getRandomArrayElement, getRandomInt, getRandomStatus} from "./functionHelpers/randomizers";
import {getRejectionReason} from "./functionHelpers/getRejectionReason";
import {IAccount} from "../models/Account";
import {IStockRequest} from "../models/StockRequest";
import {getRandomCompany} from "./functionHelpers/getRandomCompany";

export function createStockFixtures(
    itemAccount: IAccount[],
    arrayFrontOffice: mongoose.Types.ObjectId[],
    arrayTrader: mongoose.Types.ObjectId[],
): IStockRequest[]{
//#####################################################################################################
    const innerArray: IStockRequest[] = [];
    for(const iAccount of itemAccount){
        // "Pending" | "FrontWIP" | "TraderWIP" |
        // "Approved" | "Declined" | "Completed" |
        // "PartiallyCompleted";
        const status = getRandomStatus(["Pending","Pending","Pending", "FrontWIP", "TraderWIP", "Approved", "Declined", "Completed",
            "PartiallyCompleted",
            "PartiallyCompleted",
            "PartiallyCompleted",
            "PartiallyCompleted"
        ]);
        let frontOffice = undefined;// = (status !== "Pending") ? getRandomArrayElement([...arrayFrontOffice]) : undefined;
        let trader = undefined;
        let rejectionReason = "none"

        if(status !== "Declined"){
            switch(status){
                case "FrontWIP":
                case "Approved":
                    frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                    break

                case "TraderWIP":
                case "Completed":
                case "PartiallyCompleted":
                    frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                    trader = getRandomArrayElement([...arrayTrader]);
                    break
            }
        }else{
            // на каком-то этапе было отклонение,
            // либо отклонил сотрудник фронт-офиса либо трейдер
            if(getRandomInt(1, 10) % 2 === 1){
                frontOffice = getRandomArrayElement([...arrayFrontOffice]);
            }else{
                frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                trader = getRandomArrayElement([...arrayTrader]);
            }
            rejectionReason = getRejectionReason();
        }

        const returnRandomCompany = getRandomCompany();

        innerArray.push({
            user: iAccount.user,
            userFullName: iAccount.firstName + " " + iAccount.lastName,
            status: status,
            company: returnRandomCompany[0].trim(),
            ticker: returnRandomCompany[1].trim(),
            price: getRandomInt(100, 50000),
            frontManager: frontOffice,
            trader: trader,
            stockAmount: getRandomInt(1, 50),
            rejectionReason: rejectionReason,
            date: undefined,
            stockReqType: getRandomArrayElement(["Buy", "Sell"]),
            //----------------------
            stockResult: getRandomInt(1, 50), //  Для Тейдера. Кол-во акций, которое получилось купить/продать
            stockFail: getRandomInt(1, 50), //  Для Трейдера. Кол-во акций, которое НЕ получилось купить-продать
            priceResult: getRandomInt(1, 50), //  Для Трейдера. Цена исполнения
        })
    }
    return innerArray;
}

// export interface IStockRequest{
//     user: Types.ObjectId;
//     userFullName: string;
//     status: "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted";
//     company: string;  // Название компании, например Apple
//     ticker: string;  // Тикер - код, например AAPL, "EL-RM", "OMZZ_p", "ABNB-RM", "NSC-RM", "MCO-RM", "GAZP", "MCK-RM", "SNPS-RM", "ORLY-RM", "SBER", "KDP-RM", "SBER_p", "ROSN", "AZO-RM", "MSI-RM", "STZ-RM", "IQV-RM", "AJG-RM", "IDXX-RM", "PH-RM", "FTNT-RM", "RSG-RM", "ANET-RM", "LHX-RM", "LKOH", "AMP-RM", "MRVL-RM", "CMI-RM", "AME-RM", "GMKN", "ABC-RM", "KEYS-RM", "ROK-RM", "PCG-RM", "NDAQ-RM", "ON-RM", "SIBN", "WEC-RM", "CPNG-RM", "VRSK-RM", "AWK-RM", "EFX-RM", "MTB-RM", "ULTA-RM", "TSCO-RM", "TTD-RM", "VRSN-RM", "LH-RM", "PWR-RM", "JBHT-RM",
//     price: number;  //  Цена, предлагаемая клиентом
//     stockAmount: number; //  Количество акций, запрашиваемое клиентом
//     frontManager: Types.ObjectId | undefined;
//     trader: Types.ObjectId | undefined;
//     rejectionReason: string;
//     date: SchemaDefinitionProperty<string> | undefined;
//     stockReqType: "Buy" | "Sell";  // Тип - покупка или продажа
//     //----------------------
//     stockResult: number; //  Для Тейдера. Кол-во акций, которое получилось купить/продать
//     stockFail: number; //  Для Трейдера. Кол-во акций, которое НЕ получилось купить-продать
//     priceResult: number; //  Для Трейдера. Цена исполнения
// }

