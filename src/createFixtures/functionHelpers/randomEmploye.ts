import {TUserType} from "../../DataTypes";
import mongoose from "mongoose";

export function randomFrontOffice(arrayItemUsers: TUserType[]): mongoose.Types.ObjectId []{
    const arrayFrontOffice: mongoose.Types.ObjectId [] = [];
    // получить ID всех сотрудников.
    for(const arrayItemElement of arrayItemUsers){
        if(arrayItemElement.role !== "frontOffice") continue;
        arrayFrontOffice.push(arrayItemElement._id)
    }
    return arrayFrontOffice;
}

export function randomAccountant(arrayItemUsers: TUserType[]): mongoose.Types.ObjectId []{
    const arrayAccountant: mongoose.Types.ObjectId [] = [];
    // получить ID всех сотрудников.
    for(const arrayItemElement of arrayItemUsers){
        if(arrayItemElement.role !== "accountant") continue;
        arrayAccountant.push(arrayItemElement._id)
    }
    return arrayAccountant;
}

export function randomTrader(arrayItemUsers: TUserType[]): mongoose.Types.ObjectId []{
    const arrayAccountant: mongoose.Types.ObjectId [] = [];
    // получить ID всех сотрудников.
    for(const arrayItemElement of arrayItemUsers){
        if(arrayItemElement.role !== "trader") continue;
        arrayAccountant.push(arrayItemElement._id)
    }
    return arrayAccountant;
}