type TCallBackGetRandomInt = (min: number, max: number)=>number;
//********************************************************************************
const getMyTiming = (setTime: string): number=>{
    switch(setTime){
        case "sec":
            return 1000;
        case "min":
            return (60 * 1000);
        case "hour":
            return (60 * 60 * 1000);
        case "day":
            return (24 * 60 * 60 * 1000);
        case "year":
            return (365 * 24 * 60 * 60 * 1000);
    }
    return 0
}

const getDate = (myDate: Date): string=>{
    const strDate = ("0" + myDate.getDate());
    const strMonth = ("0" + (myDate.getMonth() + 1))

    return strDate.slice(strDate.length - 2, strDate.length) + "." +
        strMonth.slice(strMonth.length - 2, strMonth.length) + "." +
        myDate.getFullYear();
}

export const getRandomDate = (callBackGetRandomInt: TCallBackGetRandomInt, yearAgo: number): string=>{
    const daysInYear = yearAgo * 365;// 365 дней в году
    let deliveryTimestamp = new Date(Date.now() - getMyTiming("day") * callBackGetRandomInt(0, daysInYear));
    const strDate = getDate(deliveryTimestamp);
    return strDate;
}

//#####################################################################################################
export const getRandomDateVariation = (
    callBackGetRandomInt: TCallBackGetRandomInt,
    dateStart: string, dateEnd: string
): string=>{
    let innerDateStart = new Date(dateStart);//   "3/27/2008"     ММ.ЧЧ.ГГГГ
    let innerDateEnd = new Date(dateEnd);//     "5/21/2022"     ММ.ЧЧ.ГГГГ

    let msStart = Date.parse(innerDateStart.toISOString());
    let msEnd = Date.parse(innerDateEnd.toISOString());

    let myDate2 = new Date(callBackGetRandomInt(msStart, msEnd));
    return getDate(myDate2);
}

//#####################################################################################################
export const addTenYears = (strDate: string): string=>{
    //'16.03.2032'
    const newYYYY = strDate.slice(strDate.length - 4, strDate.length)//2022
    const newDDMM = strDate.slice(0, 6)//16.03.
    return newDDMM + (+newYYYY + 10);
}