// EXAMPLE
// console.log( getNameOrBICOfBank("HCSKKZKA","BANK") )
// console.log( getNameOrBICOfBank("ДБ АО 'Home Credit Bank'","BIC") )
import {getRandomInt} from "./randomizers";

export const getNameOrBICOfBank = (): string[]=>{

    const arrayBIC = [
        ["HCSKKZKA", "АО «Жилищный строительный сберегательный банк «Отбасы банк»"],
        ["KZIBKZKA", "АО «Дочерний банк «Казахстан - Зираат интернешнл банк»"],
        ["ATYNKZKA", "АО «Altyn Bank» (ДБ China Citic Bank Corporation Ltd)"],
        ["ICBKKZKX", "АО «Торгово-промышленный Банк Китая в Алматы»"],
        ["HSBKKZKX", "АО «Народный сберегательный банк Казахстана»"],
        ["NBPAKZKA", "АО ДБ «Национальный Банк Пакистана»"],
        ["KSNVKZKA", "АО «Банк Фридом Финанс Казахстан»"],
        ["TSESKZKA", "АО «First Heartland Jýsan Bank»"],
        ["BKCHKZKA", "АО ДБ «Банк Китая в Казахстане»"],
        ["ZAJSKZ22", "АО «Исламский банк «Заман-Банк»"],
        ["HLALKZKZ", "АО «Исламский Банк «Al-Hilal»"],
        ["SHBKKZKA", "АО «Шинхан Банк Казахстан»"],
        ["VTBAKZKZ", "ДО АО Банк ВТБ (Казахстан)"],
        ["INLMKZKA", "ДБ АО 'Home Credit Bank'"],
        ["CITIKZKA", "АО «Ситибанк Казахстан»"],
        ["ALFAKZKA", "АО ДБ «Есо Center Bank»"],
        ["KCJBKZKX", "АО «Банк ЦентрКредит»"],
        ["EURIKZKA", "АО «Евразийский Банк»"],
        ["KINCKZKA", "АО «Банк «Bank RBK»"],
        ["BRKEKZKA", "АО «Bereke Bank»"],
        ["CASPKZKA", "АО «Kaspi Bank»"],
        ["IRTYKZKA", "АО «ForteBank»"],
        ["NURSKZKX", "АО «Нурбанк»"],
    ]

    const innerIndex =  getRandomInt(0, arrayBIC.length-1)
    return arrayBIC[innerIndex];
}
