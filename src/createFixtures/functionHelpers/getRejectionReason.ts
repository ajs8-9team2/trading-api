import {getRandomArrayElement, getRandomInt} from "./randomizers";

export function getRejectionReason():string{
    return getRandomArrayElement(["ой все!",
        "бил меня в детстве в школе", "не разбирается в мемах",
        "не любит кошек", "любит маринованные соленные арбузы",
        "сын маминой подруги, которого мама ставит в пример", "бывшая жена, которая отсудила у меня мою собаку",
        "продал мне пробники, которые оказались бесплатными", "бывший работодатель, который заставлял работать в шаббат по субботам",
        "какой-то сатанист", "рассказывает анекдоты с серьезным лицом",
        "утверждает, что МММ не финансовая пирамида, а Мавроди галактический посланник"
    ])
}