export function getRandomArrayElement<T>(arr: T[]): T{
    return arr[Math.floor(Math.random() * arr.length)]
}

export function getRandomInt(min: number, max: number){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

type status1 = "Pending" | "WorkInProgress" | "Approved" | "Declined"
type status2 = "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
type status3 = "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted";

export function getRandomStatus<T extends status1 | status2 | status3>(arr: T[]): T{
    return arr[Math.floor(Math.random() * arr.length)]
}