import * as mongoose from "mongoose";
import {addTenYears, getRandomDate, getRandomDateVariation} from "./functionHelpers/getRandomDate";
import {getRandomArrayElement, getRandomInt, getRandomStatus} from "./functionHelpers/randomizers";
import {TPickUser, TUserType} from "../DataTypes";
import {getFIO} from "./functionHelpers/getFIO";
import {IAccReq} from "../models/AccRequest";
import {getAddress} from "./functionHelpers/getAdress";
import {getRejectionReason} from "./functionHelpers/getRejectionReason";

// export function createAccRecFixtures(arrayItem: (TUser & { _id: mongoose.Types.ObjectId })[]): IAccReq[]{
export function createAccRecFixtures(arrayItem: TUserType[],arrayFrontOffice:mongoose.Types.ObjectId[]): IAccReq[]{
    // const arrayFrontOffice: mongoose.Types.ObjectId [] = [];
    // // получить ID всех сотрудников.
    // for(const arrayItemElement of arrayItem){
    //     if(arrayItemElement.role !== "frontOffice") continue;
    //     arrayFrontOffice.push(arrayItemElement._id)
    // }
//#####################################################################################################
    const innerArray: IAccReq[] = [];
    for(const arrayItemElement of arrayItem){
        if(arrayItemElement.role !== "client") continue;

        const arrayFIO = getFIO();
        //ММ.ДД.ГГГГ
        const dateOfBirth: string = getRandomDateVariation(getRandomInt, "3/27/1970", "5/21/2002");

        const identificationNumber: string =
            dateOfBirth[8] + dateOfBirth[9] +
            dateOfBirth[3] + dateOfBirth[4] +
            dateOfBirth[0] + dateOfBirth[1] + "123456"

        //для стран
        const rndmCountry: number = getRandomInt(0, 24);//arrCountry.length - 1)

        const dateOfIssue = getRandomDate(getRandomInt, 5);//дата выдачи Удост.
        const expirationDate = addTenYears(dateOfIssue);//дата окончания Удост.

        // если у клиента есть аккаунт, значит статус клиентской заявки "Approved"
        const status = (arrayItemElement.isHasAccount) ? "Approved" : getRandomStatus(["Pending", "WorkInProgress", "Declined"]);
        // если у клиентской заявки  статус не равно "Pending", значит за ней закреплен сотрудник фронт-офиса
        const assignedTo = (status !== "Pending") ? getRandomArrayElement([...arrayFrontOffice]) : undefined;
        const rejectionReason = (status === "Declined") ? getRejectionReason() : "none";

        innerArray.push({
            date: undefined,
            // requestType: "",
            user: arrayItemElement._id,
            status: status,
            firstName: arrayFIO[0],
            lastName: arrayFIO[1],
            patronym: arrayFIO[2],
            dateOfBirth: dateOfBirth,
            identificationNumber: identificationNumber,
            email: arrayItemElement.email,
            phone: (()=>{
                return `+7(${getRandomArrayElement(["707", "777", "702"])})${getRandomInt(1_000_000, 9_999_999)}`
            })() as string,

            country: getAddress(rndmCountry, 0), //arrCountry[rndmCountry][0],
            area: getAddress(rndmCountry, 1), //arrCountry[rndmCountry][1],
            city: getAddress(rndmCountry, 2), //arrCountry[rndmCountry][2],
            address: getAddress(rndmCountry, 3), //arrCountry[rndmCountry][3],
            // date: getRandomDate(getRandomInt, 1),// дата выбирается в диапазоне от текущей даты и минус 1 год назад

            authority: getRandomArrayElement(["МВД РК", "Министерство Юстиции РК"]),
            dateOfIssue: dateOfIssue,
            expirationDate: expirationDate,
            documentFront: getRandomArrayElement(["passport1.jpg", "passport2.jpg", "passport3.jpg", "passport4.jpg", "passport5.jpg"]),
            documentBack: getRandomArrayElement(["passport1.jpg", "passport2.jpg", "passport3.jpg", "passport4.jpg", "passport5.jpg"]),
            rejectionReason: rejectionReason,
            // assignedTo:getRandomArrayElement([...arrayFrontOffice.map(item=>{
            //     return item._id
            // })]),
            assignedTo: assignedTo,
            idCardNumber: "" + getRandomInt(100, 999) + "" + getRandomInt(100, 999) + "" + getRandomInt(100, 999),
            citizenship: getRandomArrayElement(["resident", "resident", "resident", "resident", "resident", "resident", "not-resident"])
        });
    }
    return innerArray;
}

/*
{
    _id: ObjectId("638390effa233443badd7278"),
    user: ObjectId("638390ecfa233443badd7236"),
    status: 'WorkInProgress',
    firstName: 'Гайдин',
    lastName: 'Брунсов',
    patronym: 'Дидеричович',
    dateOfBirth: '29.10.2001',
    identificationNumber: '011029123456',
    email: 'client16@mail.com',
    phone: '87771357823',
    country: 'Республика Казахстан',
    area: 'Кокшетау область',
    city: 'Актау город',
    address: 'Бейбитшилик переулок, 812',
    date: '10.10.2022',
    authority: 'Министерство Юстиции РК',
    dateOfIssue: '07.05.2021',
    expirationDate: '07.05.2031',
    documentFront: 'passport3.jpg',
    documentBack: 'passport5.jpg',
    rejectionReason: 'none',
    assignedTo: ObjectId("638390ecfa233443badd723b"),
    idCardNumber: '754546630',
    __v: 0
},
*/