import {nanoid} from "nanoid";
import {getRandomInt} from "./functionHelpers/randomizers";
import {TPickUser} from "../DataTypes";
import {getFIO} from "./functionHelpers/getFIO";



function createClient(amount: number): TPickUser[]{
    const clientArray: Array<TPickUser> = [];
    for(let i1 = 0; i1 < amount; i1++){
        clientArray.push({
            email: `client${i1}@mail.com`,
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: (()=>{
                return (getRandomInt(1, 10) % 2 === 1);
            })()
        });
    }
    return clientArray;
}

//===========================================================
function createFrontManager(amount: number): TPickUser[]{
    const frontManager: Array<TPickUser> = [];  
    for(let i1 = 0; i1 < amount; i1++){
        const arrayFIO = getFIO();
        frontManager.push({
            email: `front${i1}@mail.com`,
            password: "password",
            role: "frontOffice",
            token: nanoid(),
            isHasAccount: false,
            fullName: arrayFIO[0] + " " + arrayFIO[1],
        });
    }
    return frontManager;
}

//===========================================================
/*export */
function createBackManager(amount: number): TPickUser[]{
    const backManager: Array<TPickUser> = [];
    for(let i1 = 0; i1 < amount; i1++){
        const arrayFIO = getFIO();
        backManager.push({
            email: `back${i1}@mail.com`,
            password: "password",
            role: "backOffice",
            token: nanoid(),
            isHasAccount: false,
            fullName: arrayFIO[0] + " " + arrayFIO[1],
        });
    }
    return backManager;
}

//===========================================================
function createTrader(amount: number): TPickUser[]{
    const trader: Array<TPickUser> = [];
    for(let i1 = 0; i1 < amount; i1++){
        const arrayFIO = getFIO();
        trader.push({
            email: `trader${i1}@mail.com`,
            password: "password",
            role: "trader",
            token: nanoid(),
            isHasAccount: false,
            fullName:arrayFIO[0] + " " + arrayFIO[1],
        });
    }
    return trader;
}
//===========================================================
function createAccountant(amount: number): TPickUser[]{
    const accountant: Array<TPickUser> = [];
    for(let i1 = 0; i1 < amount; i1++){
        const arrayFIO = getFIO();
        accountant.push({
            email: `accountant${i1}@mail.com`,
            password: "password",
            role: "accountant",
            token: nanoid(),
            isHasAccount: false,
            fullName: arrayFIO[0] + " " + arrayFIO[1],
        });
    }
    return accountant;
}
//===========================================================
const arrayFIO = getFIO();
export const allUsersFunction = (
    amountClient: number,
    amountFrontManager: number,
    amountBackManager: number,
    amountTrader: number,
    amountAccountant: number,
): TPickUser[]=>{
    const allUsers: TPickUser[] = [
        ...createClient(amountClient),
        ...createFrontManager(amountFrontManager),
        ...createBackManager(amountBackManager),
        ...createTrader(amountTrader),
        ...createAccountant(amountAccountant),
        {
            email: "admin@mail.com",
            password: "password",
            role: "admin",
            token: nanoid(),
            isHasAccount: false,
            fullName: arrayFIO[0] + " " + arrayFIO[1],
        }
    ]
    return allUsers;
}
//===========================================================