import {getRandomArrayElement, getRandomInt, getRandomStatus} from "./functionHelpers/randomizers";
import {IDepositReq} from "../models/DepositRequest";
import {getFIO} from "./functionHelpers/getFIO";
import {getNameOrBICOfBank} from "./functionHelpers/getNameOrBICOfBank";
import {IAccReq} from "../models/AccRequest";
import {getRandomDateVariation} from "./functionHelpers/getRandomDate";
import {getRejectionReason} from "./functionHelpers/getRejectionReason";
import {IClientAccount} from "../models/ClientAccount";
import mongoose from "mongoose";

//export function createAccountFixtures(arrayItemAccReq: IAccReq[]): IAccount[]{

// export type TDeposit = {
//     user: mongoose.Types.ObjectId | string;
//     status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed";
//     senderIIN: string;
//     senderIIK: string;
//     senderBIK: string;
//     senderBank: string;
//     senderKBe: string;
//     senderFullName: string;
//     frontManager: mongoose.Types.ObjectId | string,
//     accountant: mongoose.Types.ObjectId | string | undefined,
//     rejectionReason: string;
//     amount: number;
//     date?: string;
// }


export function createDepositFixtures(
    // arrayItemAccReq: IAccReq[],
    arrayItemClientAccount: IClientAccount[],
    arrayFrontOffice: mongoose.Types.ObjectId[],
    arrayAccountant: mongoose.Types.ObjectId[]
): IDepositReq[]{
    const innerArray: IDepositReq[] = [];

    for(const iClientAccount of arrayItemClientAccount){
        if(iClientAccount.isMyIIK !== true) continue;

        const dateOfBirth: string = getRandomDateVariation(getRandomInt, "3/27/1970", "5/21/2002");

        const identificationNumber: string =
            dateOfBirth[8] + dateOfBirth[9] +
            dateOfBirth[3] + dateOfBirth[4] +
            dateOfBirth[0] + dateOfBirth[1] + "123456"


        const returnNameOfBankOrBIC = getNameOrBICOfBank();
        const arrayFIO = getFIO();

        const status = getRandomStatus(["Pending", "Pending", "Pending", "Pending", "Pending", "Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]);
        let frontOffice = undefined;// = (status !== "Pending") ? getRandomArrayElement([...arrayFrontOffice]) : undefined;
        let accountant = undefined;
        let rejectionReason = "none"

        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        if(status !== "Declined"){
            switch(status){
                case "FrontWIP":
                case "Approved":
                    frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                    break

                case "AccountantWIP":
                case "Completed":
                    frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                    accountant = getRandomArrayElement([...arrayAccountant]);
                    break
            }
        }else{
            if(getRandomInt(1, 10) % 2 === 1){
                frontOffice = getRandomArrayElement([...arrayFrontOffice]);
            }else{
                frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                accountant = getRandomArrayElement([...arrayAccountant]);
            }
            rejectionReason = getRejectionReason();
        }

        innerArray.push({
            date: undefined,
            user: iClientAccount.user,
            status: status,
            senderIIN: identificationNumber,
            senderIIK: `KZ${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}`,
            senderBIK: returnNameOfBankOrBIC[0],
            senderBank: returnNameOfBankOrBIC[1],
            senderKBe: "17",
            senderFullName: arrayFIO[0] + " " + arrayFIO[1] + " " + arrayFIO[2],
            frontManager: frontOffice,
            accountant: accountant,
            rejectionReason: rejectionReason,
            amount: getRandomInt(10000, 99999),
            // date:
        })
    }

    // for(const arrayItemElement of arrayItemAccReq){
    //     if(arrayItemElement.status !== "Approved") continue;
    //
    //     const dateOfBirth: string = getRandomDateVariation(getRandomInt, "3/27/1970", "5/21/2002");
    //
    //     const identificationNumber: string =
    //         dateOfBirth[8] + dateOfBirth[9] +
    //         dateOfBirth[3] + dateOfBirth[4] +
    //         dateOfBirth[0] + dateOfBirth[1] + "123456"
    //
    //
    //     const returnNameOfBankOrBIC = getNameOrBICOfBank();
    //     const arrayFIO = getFIO();
    //
    //     const status = getRandomStatus(["Pending","Pending","Pending","Pending","Pending","Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]);
    //     const assignedTo = (status !== "Pending") ? arrayItemElement.assignedTo! : undefined;
    //     const rejectionReason = (status === "Declined") ? getRejectionReason() : "none";
    //
    //     innerArray.push({
    //         date: undefined,
    //         user: arrayItemElement.user,
    //         status: status,
    //         senderIIN: identificationNumber,
    //         senderIIK: `KZ${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}`,
    //         senderBIK: returnNameOfBankOrBIC[0],
    //         senderBank: returnNameOfBankOrBIC[1],
    //         senderKBe: "17",
    //         senderFullName: arrayFIO[0] + " " + arrayFIO[1] + " " + arrayFIO[2],
    //         frontManager: assignedTo,
    //         accountant: undefined,
    //         rejectionReason: rejectionReason,
    //         amount: getRandomInt(10000, 99999)
    //         // date:
    //     })
    // }

    return innerArray
}
