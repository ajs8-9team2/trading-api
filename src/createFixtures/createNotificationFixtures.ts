import {TUserType} from "../DataTypes";
import {INotification} from "../models/Notification";

export function createNotificationFixtures(arrayUser:TUserType[]): INotification[]{
    const innerArray: INotification[] = [];
    // export function randomFrontOffice(arrayItemUsers: TUserType[]): mongoose.Types.ObjectId []{
    //     const arrayFrontOffice: mongoose.Types.ObjectId [] = [];
    //     // получить ID всех сотрудников.
    //     for(const arrayItemElement of arrayItemUsers){
    //         if(arrayItemElement.role !== "frontOffice") continue;
    //         arrayFrontOffice.push(arrayItemElement._id)
    //     }
    //     return arrayFrontOffice;
    // }

    for(const arrayItemElement of arrayUser){
        if(arrayItemElement.role !== "client") continue;
        innerArray.push({
            user:arrayItemElement._id,
            userNotifications:[]
        })
    }
    return innerArray;
}
