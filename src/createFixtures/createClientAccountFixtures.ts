import {getRandomArrayElement, getRandomInt} from "./functionHelpers/randomizers";
import {getFIO} from "./functionHelpers/getFIO";
import {IAccReq} from "../models/AccRequest";
import {IClientAccount} from "../models/ClientAccount";
import {getNameOrBICOfBank} from "./functionHelpers/getNameOrBICOfBank";
import {IDepositReq} from "../models/DepositRequest";
import {TUserType} from "../DataTypes";
import {IAccount} from "../models/Account";






// export function createAccountFixtures(arrayItem: IAccReq[],arrayUser:TUserType[]): IAccount[]{
//     const innerArray: IAccount[] = [];
//
//     for(const arrayItemElement of arrayItem){
//         if(arrayItemElement.status !== "Approved") continue;
//         let isFlag = false;
//         for(const userElement of arrayUser){
//             if(arrayItemElement.user._id === userElement._id)
//                 isFlag = userElement.isHasAccount
//         }
//         // если нет аккаунта, то пропускаем цикл
//         if(!isFlag)continue;








export function createClientAccountFixtures(arrayItemAccReq: IAccReq[],arrayUser:TUserType[]): IClientAccount[]{
    const innerArray: IClientAccount[] = [];

    for(const arrayItemElement of arrayItemAccReq){
        if(arrayItemElement.status !== "Approved") continue;

        let isFlag = false;
        for(const userElement of arrayUser){
            if(arrayItemElement.user._id === userElement._id)
                isFlag = userElement.isHasAccount
        }
        // если нет аккаунта, то пропускаем цикл
        if(!isFlag)continue;

        const returnNameOfBankOrBIC = getNameOrBICOfBank();

        innerArray.push({
            date: undefined,
            user: arrayItemElement.user,
            BIK: returnNameOfBankOrBIC[0],
            bank: returnNameOfBankOrBIC[1],
            // recipientName: "",
            IIK: `KZ${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}`,
            isMyIIK:getRandomArrayElement([true,true,true,true,false]),//80%
        })
    }
    return innerArray;
}
