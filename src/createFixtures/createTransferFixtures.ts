import {getRandomArrayElement, getRandomInt, getRandomStatus} from "./functionHelpers/randomizers";
import {IDepositReq} from "../models/DepositRequest";
import {getFIO} from "./functionHelpers/getFIO";
import {getNameOrBICOfBank} from "./functionHelpers/getNameOrBICOfBank";
import {IAccReq} from "../models/AccRequest";
import {getRandomDateVariation} from "./functionHelpers/getRandomDate";
import {getRejectionReason} from "./functionHelpers/getRejectionReason";
import {SchemaDefinitionProperty, Types} from "mongoose";
import {ITransferReq} from "../models/TransferRequest";
import {IClientAccount} from "../models/ClientAccount";
import mongoose from "mongoose";
import {TUserType} from "../DataTypes";


// arrayItemAccReq: IAccReq[],
// arrayItemDeposit: IDepositReq[]
export function createTransferFixtures(
    arrayItemAccReq: IAccReq[],
    arrayUser: TUserType[],
    arrayItemClientAccount: IClientAccount[],
    arrayFrontOffice: mongoose.Types.ObjectId[],
    arrayAccountant: mongoose.Types.ObjectId[],
): ITransferReq[]{
    const innerArray: ITransferReq[] = [];
    for(const arrayItemElement of arrayItemAccReq){

        if(arrayItemElement.status !== "Approved") continue;

        let isFlag = false;
        for(const userElement of arrayUser){
            if(arrayItemElement.user._id === userElement._id)
                isFlag = userElement.isHasAccount
        }
        // если нет аккаунта, то пропускаем цикл
        if(!isFlag) continue;
//--------------------------------------------------------------
        let ClientAccountObject: IClientAccount | undefined = undefined;
        for(const iClientAccount of arrayItemClientAccount){
            if(arrayItemElement.user._id === iClientAccount.user._id){
                ClientAccountObject = iClientAccount;
                break;
            }
        }

        const status = getRandomStatus(["Pending", "Pending", "Pending", "Pending", "Pending", "Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]);
        let frontOffice = undefined;// = (status !== "Pending") ? getRandomArrayElement([...arrayFrontOffice]) : undefined;
        let accountant = undefined;
        let rejectionReason = "none"

        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        if(status !== "Declined"){
            switch(status){
                case "FrontWIP":
                case "Approved":
                    frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                    break

                case "AccountantWIP":
                case "Completed":
                    frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                    accountant = getRandomArrayElement([...arrayAccountant]);
                    break
            }
        }else{
            if(getRandomInt(1, 10) % 2 === 1){
                frontOffice = getRandomArrayElement([...arrayFrontOffice]);
            }else{
                frontOffice = getRandomArrayElement([...arrayFrontOffice]);
                accountant = getRandomArrayElement([...arrayAccountant]);
            }
            rejectionReason = getRejectionReason();
        }


        const dateOfBirth: string = getRandomDateVariation(getRandomInt, "3/27/1970", "5/21/2002");
        const identificationNumber: string =
            dateOfBirth[8] + dateOfBirth[9] +
            dateOfBirth[3] + dateOfBirth[4] +
            dateOfBirth[0] + dateOfBirth[1] + "123456"

        const returnNameOfBankOrBIC_1 = getNameOrBICOfBank();
        const returnNameOfBankOrBIC_2 = getNameOrBICOfBank();
        const arrayFIO = getFIO();

        innerArray.push({
            user: arrayItemElement.user,
            status: status,
            senderIIN: arrayItemElement.identificationNumber,
            recipientIIN: identificationNumber,
            senderIIK: `KZ${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}`,
            recipientIIK: `KZ${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}${getRandomInt(100_000, 999_999)}`,

            senderBIK: ClientAccountObject?.BIK!,//returnNameOfBankOrBIC_1[0],
            senderBank: ClientAccountObject?.bank!,

            recipientBIK: returnNameOfBankOrBIC_2[0],
            recipientBank: returnNameOfBankOrBIC_2[1],

            senderKBe: "17",
            recipientKBe: "17",

            senderFullName: arrayItemElement.firstName + " " + arrayItemElement.lastName + " " + arrayItemElement.patronym,
            recipientFullName: arrayFIO[0] + " " + arrayFIO[1] + " " + arrayFIO[2],

            frontManager: frontOffice,
            accountant: accountant,
            amount: getRandomInt(10000, 99999),
            date: undefined,
            rejectionReason: rejectionReason,
            // requestType? : string; // для чего requestType???
        })
    }
    return innerArray
}

// user: Types.ObjectId;
// status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed";
// senderIIN: string;
// recipientIIN: string;
// senderIIK: string;
// recipientIIK: string;
// senderBIK: string;
// recipientBIK: string;
// senderBank: string;
// recipientBank: string;
// senderKBe: string;
// recipientKBe: string;
// senderFullName: string;
// recipientFullName: string;
// frontManager: Types.ObjectId | undefined;
// accountant: Types.ObjectId | undefined;
// amount: number;
// date: SchemaDefinitionProperty<string> | undefined;
// rejectionReason?: string;
// requestType?: string; // для чего requestType???