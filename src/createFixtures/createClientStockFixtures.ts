import {IStockRequest} from "../models/StockRequest";
import {IClientStock} from "../models/ClientStock";

export function createClientStockFixtures(
    arrayStockFixtures: IStockRequest[]
): IClientStock[]{
//#####################################################################################################
    const innerArray: IClientStock[] = [];
    for(const arrayStockFixture of arrayStockFixtures){
        if(arrayStockFixture.status !== "PartiallyCompleted" &&
            arrayStockFixture.status !== "Completed" ) continue

        innerArray.push({
            user: arrayStockFixture.user,
            company: arrayStockFixture.company,
            ticker: arrayStockFixture.ticker,
            amount: arrayStockFixture.stockAmount,
        })
    }
    return innerArray;
}