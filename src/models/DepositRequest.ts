import {Schema, Types, SchemaDefinitionProperty, model} from 'mongoose';

export interface IDepositReq{
    user: Types.ObjectId;
    status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed";
    senderIIN: string;
    senderIIK: string;
    senderBIK: string;
    senderBank: string;
    senderKBe: string;
    senderFullName: string;
    frontManager: Types.ObjectId | undefined;
    accountant: Types.ObjectId | undefined;
    rejectionReason: string;
    amount: number;
    date: SchemaDefinitionProperty<string> | undefined;
    requestType?:string; // для чего requestType???
}

const DepositReqSchema = new Schema<IDepositReq>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: [
            "Pending", "FrontWIP",
            "AccountantWIP", "Approved",
            "Declined", "Completed"]
    },
    amount: {
        type: Number,
        required: [true, "Money amount is required"]
    },
    senderFullName: {
        type: String,
        required: [true, "Sender's full name is required"]
    },
    senderIIN: {
        type: String,
        required: [true, "Sender's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    senderBank: {
        type: String,
        required: [true, "Sender's Bank is required"]
    },
    senderIIK: {
        type: String,
        required: [true, "Sender's IIK is required"]
    },
    senderBIK: {
        type: String,
        required: [true, "Sender's BIK is required"]
    },
    senderKBe: {
        type: String,
        required: [true, "Sender's KBe is required"]
    },
    frontManager: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    accountant: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    rejectionReason: {
        type: String,
    },
    requestType: {
        type: String,
        required: true,
        default: "Deposit request",
        enum: ["Deposit request"]
    },
    date: {
        type: Date,
        default: new Date()
    }
});

const DepositRequest = model<IDepositReq>("DepositRequest", DepositReqSchema);

export default DepositRequest;