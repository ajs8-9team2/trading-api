import {Schema, Types, SchemaDefinitionProperty, model} from 'mongoose';

export interface IClientAccount{
    user: Types.ObjectId;
    IIK: string;
    BIK: string;
    bank: string;
    date: SchemaDefinitionProperty<string> | undefined;
    // recipientName?: string;
    isMyIIK: boolean;
}

const ClientAccountSchema = new Schema<IClientAccount>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    BIK: {
        type: String,
        required: [true, "BIK is required"]
    },
    bank: {
        type: String,
        required: [true, "Bank is required"]
    },
    // recipientName: {
    //     type: String,
    // },
    IIK: {
        type: String,
        required: [true, "IIK is required"]
    },
    date: {
        type: Date,
        default: new Date()
    },
    isMyIIK: {
        type: Boolean,
        required: [true, "isMyIIK is required"]
    },
});

const ClientAccount = model<IClientAccount>("ClientAccount", ClientAccountSchema);

export default ClientAccount;