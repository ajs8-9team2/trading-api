import {Schema, Types, SchemaDefinitionProperty, model} from 'mongoose';

export interface IStockRequest{
    user: Types.ObjectId;
    userFullName: string; 
    status: "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted";
    company: string;  // Название компании, например Apple
    ticker: string;  // Тикер - код, например AAPL
    price: number;  //  Цена, предлагаемая клиентом
    stockAmount: number; //  Количество акций, запрашиваемое клиентом
    frontManager: Types.ObjectId | undefined;
    trader: Types.ObjectId | undefined;
    rejectionReason: string;
    date: SchemaDefinitionProperty<string> | undefined;
    stockReqType: "Buy" | "Sell";  // Тип - покупка или продажа
    //---------------------- 
    stockResult: number; //  Для Тейдера. Кол-во акций, которое получилось купить/продать
    stockFail: number; //  Для Трейдера. Кол-во акций, которое НЕ получилось купить-продать
    priceResult: number; //  Для Трейдера. Цена исполнения
}

const StockReqSchema = new Schema<IStockRequest>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "Необходим ID пользователя"]
    },
    userFullName: {
        type: String,
        required: [true, "Необходимо имя пользователя"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: [
            "Pending", "FrontWIP",
            "TraderWIP", "Approved",
            "Declined", "Completed", 
            "PartiallyCompleted"]
    },
    stockAmount: {
        type: Number,
        required: [true, "Необходимо количество акций"]
    },
    company: {
        type: String,
        required: [true, "Необходимо наименование акции"]
    },
    ticker: {
        type: String,
        required: [true, "Необходимо тикер акции"]
    },
    price: {
        type: Number,
        required: [true, "Необходима цена акции"]
    },
    frontManager: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    trader: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    rejectionReason: {
        type: String,
    },
    stockReqType: {
        type: String,
        required: [true, "Необходимо выбрать тип действия: покупка или продажа"],
        // default: "Buy",
        enum: ["Buy", "Sell"]
    },
    date: {
        type: Date,
        default: new Date()
    },
    stockResult: {
        type: Number
    },
    stockFail: {
        type: Number
    },
    priceResult: {
        type: Number
    }
});

const StockRequest = model<IStockRequest>("StockRequest", StockReqSchema);

export default StockRequest;