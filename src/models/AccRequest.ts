// import mongoose, { Schema } from "mongoose";
import {Schema, Types, SchemaDefinitionProperty, model} from 'mongoose';

export interface IAccReq{
    user: Types.ObjectId;
    status: "Pending" | "WorkInProgress" | "Approved" | "Declined";
    firstName: string;
    lastName: string;
    patronym: string,
    dateOfBirth: string;
    identificationNumber: string;
    email: string;
    phone: string;
    country: string;
    city: string;
    area: string;
    address: string;
    date: SchemaDefinitionProperty<string> | undefined;
    authority: string;
    dateOfIssue: string;
    expirationDate: string;
    documentFront: string;
    documentBack: string;
    assignedTo: Types.ObjectId | undefined;
    idCardNumber: string;
    citizenship: "resident" | "not-resident";
    rejectionReason?: string;
    requestType?: string; // для чего requestType???
}

const AccReqSchema = new Schema<IAccReq>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "Укажите ID пользователя"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "WorkInProgress", "Approved", "Declined"]
    },
    firstName: {
        type: String,
        required: [true, "Необходимо имя клиента"]
    },
    lastName: {
        type: String,
        required: [true, "Необходима фамилия клиента"]
    },
    patronym: {
        type: String
    },
    dateOfBirth: {
        type: String,
        required: [true, "Необходима дата рождения клиента"]
    },
    identificationNumber: {
        type: String,
        required: [true, "Необходим ИИН клиента"],
        minlength: 12,
        maxlength: 12
    },
    email: {
        type: String,
        required: [true, "Необходима почта клиента"]
    },
    phone: {
        type: String
    },
    country: {
        type: String
    },
    area: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    authority: {
        type: String,
        required: [true, "Необходимо поле кем выдано удостоверение"]
    },
    dateOfIssue: {
        type: String,
        required: [true, "Необходима дата выдачи удостоверения"]
    },
    expirationDate: {
        type: String,
        required: [true, "Необходима дата действия удостоверения"]
    },
    documentFront: {
        type: String,
        required: [true, "Необходимо загрузить первую страницу удостоверения"]
    },
    documentBack: {
        type: String,
        required: [true, "Необходимо загрузить первую страницу удостоверения"]
    },
    requestType: {
        type: String,
        required: true,
        default: "Account Request",
        enum: ["Account Request"]
    },
    rejectionReason: {
        type: String
    },
    assignedTo: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    idCardNumber: {
        type: String,
        required: [true, "Необходим номер удостоверения"]
    },
    date: {
        type: Date,
        default: new Date()
    },
    citizenship: {
        type: String,
        required: [true, "Укажите гражданство пользователя"],
        enum: ["resident", "not-resident"]
    },
});


const AccRequest = model<IAccReq>("AccRequest", AccReqSchema);

export default AccRequest;