import {Schema, Types, SchemaDefinitionProperty, model} from 'mongoose';

export interface ITransferReq {
    user: Types.ObjectId;
    status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed";
    senderIIN: string;
    recipientIIN: string;
    senderIIK: string;
    recipientIIK: string;
    senderBIK: string;
    recipientBIK: string;
    senderBank: string;
    recipientBank: string;
    senderKBe: string;
    recipientKBe: string;
    senderFullName: string;
    recipientFullName: string;
    frontManager: Types.ObjectId | undefined;
    accountant: Types.ObjectId | undefined;
    amount: number;
    date: SchemaDefinitionProperty<string> | undefined;
    rejectionReason?: string;
    requestType?: string; // для чего requestType???
}

const TransferReqSchema = new Schema<ITransferReq>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
    },
    amount: {
        type: Number,
        required: [true, "Money amount is required"]
    },
    senderFullName: {
        type: String,
        required: [true, "Sender's full name is required"]
    },
    recipientFullName: {
        type: String,
        required: [true, "Recipient's full name is required"]
    },
    senderIIN: {
        type: String,
        required: [true, "Sender's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    recipientIIN: {
        type: String,
        required: [true, "Recipient's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    senderBank: {
        type: String,
        required: [true, "Sender's Bank is required"]
    },
    recipientBank: {
        type: String,
        required: [true, "Recipient's Bank is required"]
    },
    senderIIK: {
        type: String,
        required: [true, "Sender's IIK is required"]
    },
    recipientIIK: {
        type: String,
        required: [true, "Recipient's IIK is required"]
    },
    senderBIK: {
        type: String,
        required: [true, "Sender's BIK is required"]
    },
    recipientBIK: {
        type: String,
        required: [true, "Recipient's BIK is required"]
    },
    senderKBe: {
        type: String,
        required: [true, "Sender's KBe is required"]
    },
    recipientKBe: {
        type: String,
        required: [true, "Recipient's KBe is required"]
    },
    frontManager: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    accountant: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    rejectionReason: {
        type: String,
    },
    requestType: {
        type: String,
        required: true,
        default: "Transfer request"
    },
    date: {
        type: Date,
        default: new Date()
    }
});

const TransferRequest = model<ITransferReq>("TransferRequest", TransferReqSchema);

export default TransferRequest;