import {model, Schema, Types} from 'mongoose';

export interface IAccount{
    user: Types.ObjectId;
    accountNumber: string;
    accountType: string;
    firstName: string;
    lastName: string;
    patronym: string;
    dateOfBirth: string;
    identificationNumber: string;
    email: string;
    phone: string;
    country: string;
    city: string;
    area: string;
    address: string;
    documentFront: string;
    documentBack: string;
    authority: string;
    dateOfIssue: string;
    expirationDate: string;
    idCardNumber: string;
    amount: number;
    citizenship: "resident" | "not-resident";
}

const AccountSchema = new Schema<IAccount>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    accountNumber: {
        type: String,
        required: true,
    },
    accountType: {
        type: String,
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    dateOfBirth: {
        type: String,
        required: true
    },
    identificationNumber: {
        type: String,
        required: true,
        minlength: 12,
        maxlength: 12
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    patronym: {
        type: String
    },
    country: {
        type: String
    },
    area: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    documentFront: {
        type: String,
        required: true
    },
    documentBack: {
        type: String,
        required: true
    },
    authority: {
        type: String,
        required: true
    },
    dateOfIssue: {
        type: String,
        required: true
    },
    expirationDate: {
        type: String,
        required: true
    },
    idCardNumber: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    citizenship: {
        type: String,
        required: [true, "Укажите гражданство пользователя"],
        enum: ["resident", "not-resident"]
    },
});

const Account = model<IAccount>("Account", AccountSchema);

export default Account;