import {model, Schema, SchemaDefinitionProperty, Types} from 'mongoose';

export type TNoticeObj = {
    date?: SchemaDefinitionProperty<string> | undefined;
    notice: string;
    // status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed";// статус заявки
    status: "Pending" | "FrontWIP" | "AccountantWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted";// статус заявки
    typeOfRequest: "accRequest" | "depositRequest" | "transferRequest" | "stockRequest";//вид заявки
    isWasRead: boolean;
    requestID: string,//просто строка, объект заявок не приклеиваю
}

export interface INotification{
    user: Types.ObjectId;
    userNotifications?: TNoticeObj[];
}

const NotificationSchema = new Schema<INotification>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    userNotifications: [{
        date: {
            type: Date,
            default: new Date()
        },
        notice: {
            type: String,
            required: true
        },
        isWasRead: {
            type: Boolean,
            required: true,
            default: false,
        },
        status: {
            type: String,
            required: true,
            // enum: ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
            enum: ["Pending", "FrontWIP", "AccountantWIP", "TraderWIP", "Approved", "Declined", "Completed", "PartiallyCompleted"]
        },
        typeOfRequest: {
            type: String,
            required: true,
            // enum: ["accRequest", "depositRequest", "transferRequest"]
            enum: ["accRequest", "depositRequest", "transferRequest", "stockRequest"]
        },
        requestID: {
            type: String,
            required: true
        }
    }]
});

const Notification = model<INotification>("Notification", NotificationSchema);

export default Notification;