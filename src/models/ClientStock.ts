import {model, Schema, Types} from 'mongoose';

export interface IClientStock{
    user: Types.ObjectId;
    company: string;  // Название, например Apple
    ticker: string;   //  Тикер - код, например AAPL
    amount: number;  // Количество акций
}

const ClientStockSchema = new Schema<IClientStock>({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    company: {
        type: String,
        required: true
    },
    ticker: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    }
});

const ClientStock = model<IClientStock>("ClientStock", ClientStockSchema);

export default ClientStock;