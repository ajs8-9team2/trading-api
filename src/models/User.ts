import bcrypt from "bcrypt";
import {nanoid} from "nanoid";
import {Schema, Types, model} from 'mongoose';

const saltWorkFactor = 10;

export interface IUser{
    email: string;
    password: string;
    role: "client"| "frontOffice"| "backOffice"| "trader"| "accountant"| "admin";
    token: string;
    account?: string;
    path?: "account-opening" | "acc-pending" | "client-office";
    isHasAccount: boolean;
    fullName?: string;

    generateToken(): Promise<string>;

    checkPassword(password: string): Promise<boolean>;

    isModified(password: string): Promise<boolean>;
}

const UserSchema = new Schema<IUser>({
    email: {
        type: String,
        required: [true, "Необходима почта"]
    },
    fullName: {
        type: String,
    },
    password: {
        type: String,
        required: [true, "Необходим пароль"]
    },
    role: {
        type: String,
        required: true,
        default: "client",
        enum: ["client", "frontOffice", "backOffice", "trader", "accountant", "admin"]
    },
    token: {
        type: String,
        required: true
    },
    account: {
        type: Schema.Types.ObjectId,
        ref: "Account",
    },
    path: {
        type: String,
        default: "account-opening",
        enum: ["account-opening", "acc-pending", "client-office"]
    },
    isHasAccount: {
        type: Boolean,
        default: false,
    },

});

// pre перед записью в базу данных данные трансформировать
UserSchema.pre("save", async function(next: (err?: Error)=>void){
    // let user = this as IUser; 18.12.22

    // if (!user.isModified("password")) return next(); 18.12.22
    if(!this.isModified("password")) return next();

    const salt = await bcrypt.genSalt(saltWorkFactor);

    // const hash = await bcrypt.hashSync(user.password, salt); 18.12.22
    const hash = await bcrypt.hashSync(this.password, salt);

    // user.password = hash; 18.12.22
    this.password = hash;

    return next();
});

UserSchema.set("toJSON", {
    transform: (doc: any, ret: any)=>{
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = async function(password: string){
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function(){
    this.token = nanoid();
};

const User = model<IUser>("User", UserSchema);

export default User;