import mongoose from "mongoose";
import {nanoid} from "nanoid";
import User, {IUser} from "./models/User";
import AccRequest from "./models/AccRequest";
import Account from "./models/Account";
import ClientAccount from "./models/ClientAccount";
import Notification from "./models/Notification";
import {allUsersFunction} from "./createFixtures/createUsersFixtures";
import {createAccRecFixtures} from "./createFixtures/createAccReqFixtures";
import {createAccountFixtures} from "./createFixtures/createAccountFixtures";
import DepositRequest from "./models/DepositRequest";
import {createDepositFixtures} from "./createFixtures/createDepositFixtures";
import {createClientAccountFixtures} from "./createFixtures/createClientAccountFixtures";
import TransferRequest from "./models/TransferRequest";
import {createTransferFixtures} from "./createFixtures/createTransferFixtures";
import {
    randomAccountant,
    randomFrontOffice,
    randomTrader,
} from "./createFixtures/functionHelpers/randomEmploye";
import {createNotificationFixtures} from "./createFixtures/createNotificationFixtures";
import {TUserType} from "./DataTypes";
import {createStockFixtures} from "./createFixtures/createStockFixtures";
import StockRequest from "./models/StockRequest";
import ClientStock from "./models/ClientStock";
import {createClientStockFixtures} from "./createFixtures/createClientStockFixtures";

mongoose.connect("mongodb://localhost/Trading");

const connection = mongoose.connection;

connection.once("open", async()=>{
    try{
        await connection.dropCollection("users");
        await connection.dropCollection("accrequests");
        await connection.dropCollection("accounts");
        await connection.dropCollection("depositrequests");
        await connection.dropCollection("clientaccounts");
        await connection.dropCollection("notifications");
        await connection.dropCollection("transferrequests");
        await connection.dropCollection("stockrequests");

        // сlientaccounts
        // accounts *
        // accrequests *
        // depositrequests *
        // transferrequests
        // users *
    }catch(e){
        console.log("Skipping drop");
    }

    // если флаг isFlag = true, то действуют мои фикстуры,
    // если флаг isFlag = false, то действуют Насти,
    const isFlag = true; // <=====

    if(isFlag){
        //-------------------------------------------------------------------------------------
        const [...itemUsers] = await User.create(allUsersFunction
            (50, 2, 2, 2, 2)
        );
        //-------------------------------------------------------------------------------------
        // await Notification.create(createNotificationFixtures(itemUsers))
        //-------------------------------------------------------------------------------------
        // массив сотрудников и пользователей у которых есть аккаунт
        const frontOffice = randomFrontOffice(itemUsers);
        const accountant = randomAccountant(itemUsers);
        const trader = randomTrader(itemUsers);
        // const users = randomUsers(itemUsers);
        // console.log(users)
        //-------------------------------------------------------------------------------------
        //1)    у заявки статус pending, то она не закреплена ни за кем!
        //2)    если статус declined, то должна быть причина отказа
        //3)    если у клиента есть аккаунт, значит статус заявки должен быть "Approved"
        const [...itemAccReq] = await AccRequest.create(createAccRecFixtures(itemUsers, frontOffice));
        //-------------------------------------------------------------------------------------
        //1)    account может быть создан, если статус заявки approved
        const [...itemAccount] = await Account.create(createAccountFixtures(itemAccReq, itemUsers));
        //-------------------------------------------------------------------------------------
        //1)    clientAccount может быть создан, если статус заявки AccReq approved
        const [...itemClientAccount] = await ClientAccount.create(createClientAccountFixtures(itemAccReq, itemUsers));
        //-------------------------------------------------------------------------------------
        // 1)    запрос на перевод денег может быть осуществлен, если статус клиентской
        //      заявки approved и имеется аккаунт
        await DepositRequest.create(createDepositFixtures(itemClientAccount, frontOffice, accountant));
        //-------------------------------------------------------------------------------------
        await TransferRequest.create(createTransferFixtures(
            itemAccReq,
            itemUsers,
            itemClientAccount,
            frontOffice,
            accountant));
        //-------------------------------------------------------------------------------------
        const [...itemStockRequest] = await StockRequest.create(createStockFixtures(itemAccount, frontOffice, trader));
        //-------------------------------------------------------------------------------------
        // создается, если статус StockRequest: PartiallyCompleted
        await ClientStock.create(createClientStockFixtures(itemStockRequest))
        //-------------------------------------------------------------------------------------
        // await createStockFixtures(arrayItem: TUserType[],arrayFrontOffice:mongoose.Types.ObjectId[])
        // await createStockFixtures(arrayItem: TUserType[],arrayFrontOffice:mongoose.Types.ObjectId[])
    }

    if(!isFlag){
        const [Client1, Client2, Client3, Client4, Client5, Client6, Front, Back, Trader, Accountant, Admin] = await User.create({
            email: "client1@mail.com",
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "client2@mail.com",
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "client3@mail.com",
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "client4@mail.com",
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "client5@mail.com",
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "client6@mail.com",
            password: "password",
            role: "client",
            token: nanoid(),
            isHasAccount: true,
        }, {
            email: "front@mail.com",
            password: "password",
            role: "frontOffice",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "back@mail.com",
            password: "password",
            role: "backOffice",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "trader@mail.com",
            password: "password",
            role: "trader",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "accountant@mail.com",
            password: "password",
            role: "accountant",
            token: nanoid(),
            isHasAccount: false,
        }, {
            email: "admin@mail.com",
            password: "password",
            role: "admin",
            token: nanoid(),
            isHasAccount: false,
        });


        await AccRequest.create(
            {
                user: Client1._id,
                status: "Pending",
                firstName: "John",
                lastName: "Doe",
                dateOfBirth: "1980-02-15",
                identificationNumber: "850226141414",
                email: "ana@tets.com",
                phone: "+77778892563",
                patronym: "Ivanov",
                country: "KZ",
                city: "Almaty",
                area: "Almaty",
                address: "Nazarbaeva",
                date: "2022-11-15",
                workStatus: "Pending",
                authority: "CFU",
                dateOfIssue: "2020-11-15",
                expirationDate: "2025-11-15",
                documentFront: "passport1.jpg",
                documentBack: "passport2.jpg",
                rejectionReason: "none",
                idCardNumber: "11111",
                citizenship: "resident"
            }, {
                user: Client2._id,
                status: "Pending",
                firstName: "Ivan",
                lastName: "Ivanov",
                dateOfBirth: "1985-11-15",
                identificationNumber: "788596141414",
                email: "ivan@tets.com",
                phone: "+77778892563",
                patronym: "Ivanov",
                country: "KZ",
                city: "Almaty",
                area: "Almaty",
                address: "Lenina",
                date: "2022-11-15",
                workStatus: "Pending",
                authority: "PRT",
                dateOfIssue: "2021-12-12",
                expirationDate: "2028-10-10",
                documentFront: "passport2.jpg",
                documentBack: "passport3.jpg",
                rejectionReason: "none",
                idCardNumber: "11111",
                citizenship: "resident"
            }, {
                user: Client3._id,
                status: "Pending",
                firstName: "Petr",
                lastName: "Petrov",
                dateOfBirth: "1984-03-03",
                identificationNumber: "788789651414",
                email: "petr@tets.com",
                phone: "+77778892563",
                patronym: "Ivanov",
                country: "KZ",
                city: "Almaty",
                area: "Almaty",
                address: "Abaya",
                date: "2022-11-15",
                workStatus: "Pending",
                authority: "QWE",
                dateOfIssue: "2008-06-12",
                expirationDate: "2028-06-10",
                documentFront: "passport3.jpg",
                documentBack: "passport4.jpg",
                rejectionReason: "none",
                idCardNumber: "33333",
                citizenship: "resident"
            }, {
                user: Client4._id,
                status: "Pending",
                firstName: "Olga",
                lastName: "Leoniova",
                dateOfBirth: "1984-03-03",
                identificationNumber: "788789651414",
                email: "olga@tets.com",
                phone: "+77778892563",
                patronym: "Ivanova",
                date: "2022-11-15",
                workStatus: "WorkInProgress",
                authority: "ASD",
                dateOfIssue: "2007-05-12",
                expirationDate: "2027-05-10",
                documentFront: "passport4.jpg",
                documentBack: "passport5.jpg",
                assignedTo: Front._id,
                idCardNumber: "33333",
                citizenship: "resident"
            }, {
                user: Client5._id,
                status: "WorkInProgress",
                firstName: "Maria",
                lastName: "Arjanceva",
                dateOfBirth: "1988-04-04",
                identificationNumber: "788789651414",
                email: "maria@tets.com",
                phone: "+77778892563",
                country: "KZ",
                city: "Almaty",
                area: "Almaty",
                address: "Nazarbaeva",
                date: "2022-11-15",
                workStatus: "WorkInProgress",
                authority: "ASD",
                dateOfIssue: "2007-05-12",
                expirationDate: "2027-05-10",
                documentFront: "passport5.jpg",
                documentBack: "passport1.jpg",
                rejectionReason: "none",
                assignedTo: Front._id,
                idCardNumber: "33333",
                citizenship: "resident"
            }, {
                user: Client6._id,
                status: "WorkInProgress",
                firstName: "Maria",
                lastName: "Arjanceva",
                dateOfBirth: "1988-04-04",
                identificationNumber: "788789651414",
                email: "maria@tets.com",
                phone: "+77778892563",
                country: "KZ",
                city: "Almaty",
                area: "Almaty",
                address: "Nazarbaeva",
                date: "2022-11-15",
                workStatus: "WorkInProgress",
                authority: "ASD",
                dateOfIssue: "2007-05-12",
                expirationDate: "2027-05-10",
                documentFront: "passport5.jpg",
                documentBack: "passport1.jpg",
                rejectionReason: "none",
                assignedTo: Front._id,
                idCardNumber: "33333",
                citizenship: "resident"

            }
        )

        /*const Account1 =*/
        await Account.create({
            accountNumber: "KZ709748396977478361",
            accountType: "KZT",
            user: Client6._id,
            firstName: "Maria",
            lastName: "Arjanceva",
            dateOfBirth: "1988-04-04",
            identificationNumber: "788789651414",
            email: "maria@tets.com",
            phone: "+77778892563",
            country: "KZ",
            area: "Almaty",
            city: "Almaty",
            address: "Nazarbaeva",
            documentFront: "passport5.jpg",
            documentBack: "passport1.jpg",
            authority: "ASD",
            dateOfIssue: "2007-05-12",
            expirationDate: "2027-05-10",
            idCardNumber: "33333",
            amount: "0",
            citizenship: "resident"
        })


        await ClientAccount.create({
                user: Client6._id,
                IIK: 'KZ656289748203850206',
                BIK: 'ALFAKZKA',
                bank: 'АО ДБ «Есо Center Bank»',
                recipientName: 'Аделхард Анселмов Джеббертович',
                userIIK: 'KZ359366248017342036',
                isMyIIK: false,
            },
            {
                user: Client5._id,
                IIK: 'KZ335127300121613770',
                BIK: 'IRTYKZKA',
                bank: 'АО «ForteBank»',
                recipientName: 'Армин Кристофов Джерхардтович',
                userIIK: 'KZ794955951940780068',
                isMyIIK: false,
            },
            {
                user: Client4._id,
                IIK: 'KZ814796889189665321',
                BIK: 'KINCKZKA',
                bank: 'АО «Банк «Bank RBK»',
                recipientName: 'Виг Алфихаров Аделалфович',
                userIIK: 'KZ533038711526982038',
                isMyIIK: false,
            },
            {
                user: Client3._id,
                IIK: 'KZ969451499190165508',
                BIK: 'CASPKZKA',
                bank: 'АО «Kaspi Bank»',
                recipientName: 'Аделхард Аделбречтов Адалстанович',
                userIIK: 'KZ896801718093819794',
                isMyIIK: false,
            },
            {
                user: Client2._id,
                IIK: 'KZ404307336273259161',
                BIK: 'KCJBKZKX',
                bank: 'АО «Банк ЦентрКредит»',
                recipientName: 'Армин Ансельмов Вендэльович',
                userIIK: 'KZ662459842430739655',
                isMyIIK: false,
            })


        // await ClientAccount.create({
        //     user: Client6._id,
        //     IBAN: "KZ86 125K ZT50 0410 0100",
        //     BIK: "HSBKKZLM",
        //     IIK: "KZ240701105HZN0000000",
        //     bank: "Kaspi Bank",
        //     recipientName: "Иван Иванов",
        //     myIBAN: false,
        // },
        // {
        //     user: Client6._id,
        //     IBAN: "KZ86 125K ZT50 0410 0139",
        //     BIK: "NOBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "Forte Bank",
        //     recipientName: "Maria Arjanceva",
        //     myIBAN: true,
        // },
        // {
        //     user: Client5._id,
        //     IBAN: "KZ86 125K ZT50 0410 0161",
        //     BIK: "HSBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "Евразийский Банк",
        //     recipientName: "Елена Николаевна",
        //     myIBAN: false,
        // },
        // {
        //     user: Client5._id,
        //     IBAN: "KZ86 125K ZT50 0410 0139",
        //     BIK: "NOBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "Forte Bank",
        //     recipientName: "Maria Arjanceva",
        //     myIBAN: true,
        // },
        // {
        //     user: Client4._id,
        //     IBAN: "KZ86 125K ZT50 0410 0127",
        //     BIK: "HSBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "Банк RBK",
        //     recipientName: "Olga Leoniova",
        //     myIBAN: true,
        // },
        // {
        //     user: Client3._id,
        //     IBAN: "KZ86 125K ZT50 0410 0132",
        //     BIK: "HSBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "Kaspi Bank",
        //     recipientName: "Petr Petrov",
        //     myIBAN: true,
        // },
        // {
        //     user: Client2._id,
        //     IBAN: "KZ86 125K ZT50 0410 0178",
        //     BIK: "HSBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "Kaspi Bank",
        //     recipientName: "Ivan Ivanov",
        //     myIBAN: true,
        // },
        // {
        //     user: Client1._id,
        //     IBAN: "KZ86 125K ZT50 0410 0100",
        //     BIK: "NOBKKZKX",
        //     IIK: "KZ240701105KSN0000000",
        //     bank: "ForteBank",
        //     recipientName: "John Doe",
        //     myIBAN: true,
        // })


        // await ClientAccount.create({
        //     user: Client6._id,
        //     IBAN: "KZ86 125K ZT50 0410 0100",
        //     BIK: "HSBKKZLM",
        //     IIK: "KZ240701105HZN0000000",
        //     bank: "Kaspi Bank",
        //     recipientName: "Иван Иванов",
        //     myIBAN: false,
        // },


        // await DepositRequest.create({
        //     user: Client1._id,
        //     status:"Pending",
        //     amount:10000,
        //     senderFullName: "John Doe",
        //     senderIIN: "850226141414",
        //     senderBank: "ForteBank",
        //     senderIIK:"KZ240701105KSN0000000",
        //     senderBIK: "HSBKKZLM",
        //     senderKBe:"17",
        //     // BIK: "NOBKKZKX",
        //     // IIK: "KZ240701105KSN0000000",
        //     // bank: "ForteBank",
        //     // recipientName: "John Doe",
        //     // myIBAN: true,
        // })


        // const depositReq = new DepositRequest({
        //     user: requestData.user,
        //     status: requestData.status,
        //     senderIIN: requestData.senderIIN,
        //     senderIIK: requestData.senderIIK,
        //     senderBIK: requestData.senderBIK,
        //     senderBank: requestData.senderBank,
        //     senderKBe: requestData.senderKBe,
        //     senderFullName: requestData.senderFullName,
        //     amount: requestData.amount
        // });

    }

    connection.close();
});

// Первая цифра – код резидентства
// 1 – резидент РК
// 2 – нерезидент РК

// Вторая цифра – сектор экономики
// 1 – Центральное правительство
// 2 – Региональные и местные органы управления
// 3 – Центральные (национальные) банки
// 4 – Другие депозитные организации
// 5 – Другие финансовые организации
// 6 – Государственные нефинансовые организации
// 7 – Негосударственные нефинансовые организации
// 8 – Некоммерческие организации
// 9 – Домашние хозяйства